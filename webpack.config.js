process.on('unhandledRejection', up => { // so that we don't fail silently
	throw up;
});

module.exports = require(
	`./webpack/webpack.${process.env.NODE_ENV || 'production'}.${process.env.TARGET || 'electron'}.config.js`
);

console.dir(module.exports, { depth: null });
