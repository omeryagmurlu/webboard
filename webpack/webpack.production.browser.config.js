const AppManifestWebpackPlugin = require('app-manifest-webpack-plugin')
const webpackConfig = require('webpack-config');
const CompressionPlugin = require('compression-webpack-plugin');
const isCI = require('is-ci');
const path = require('path');
const WorkboxPlugin = require('workbox-webpack-plugin');

const packageJson = require('../package.json');

const Config = webpackConfig.Config;
const main = path.resolve(__dirname, '..');
const pageAddress = !isCI ? '' : 'https://omeryagmurlu.gitlab.io/webboard/';

module.exports = new Config().extend(
	'webpack/partials/webpack.base.config.js',
	'webpack/partials/webpack.prod.config.js',
	'webpack/partials/webpack.browser.config.js',
).merge({
	output: {
		filename: 'browser-[name]-[chunkhash].js',
		chunkFilename: 'browser-[name]-[chunkhash].bundle.js',
		publicPath: pageAddress,
	},
	plugins: [
		new CompressionPlugin({
			asset: '[path].gz[query]',
			algorithm: 'gzip',
			test: /\.js$|\.css$|\.html$/,
			threshold: 1024,
			minRatio: 0.9
		}),
		new AppManifestWebpackPlugin({
			logo: path.resolve(main, 'build/icons/512x512.png'),
			output: '/icons-[hash:8]/', // default '/'
			prefix: pageAddress, // amina koym publicPatha baksana salak plugin
			persistentCache: true,
			inject: true,
			// favicons configuration object. Support all keys of favicons (see https://github.com/haydenbleasel/favicons)
			config: {
				appName: 'WebBoard', // Your application's name. `string`
				appDescription: 'WebBoard - Smartphone into Smartboard', // Your application's description. `string`
				developerName: packageJson.author, // Your (or your developer's) name. `string`
				developerURL: packageJson.homepage, // Your (or your developer's) URL. `string`
				background: '#fdf0ca',
				theme_color: '#fdf0ca',
				display: 'standalone', // Android display: "browser" or "standalone". `string`
				start_url: pageAddress,
				version: packageJson.version, // Your application's version number. `number`
				icons: {
					// Platform Options:
					// - offset - offset in percentage
					// - shadow - drop shadow for Android icons, available online only
					// - background:
					//   * false - use default
					//   * true - force use default, e.g. set background for Android icons
					//   * color - set background for the specified icons
					//
					android: true, // Create Android icon. `boolean` or `{ offset, background, shadow }`
					appleIcon: true, // Create Apple touch icons. `boolean` or `{ offset, background }`
					appleStartup: true, // Create Apple startup images. `boolean` or `{ offset, background }`
					coast: false, // Create Opera Coast icon. `boolean` or `{ offset, background }`
					favicons: true, // Create regular favicons. `boolean`
					firefox: false, // Create Firefox OS icons. `boolean` or `{ offset, background }`
					windows: true, // Create Windows 8 tile icons. `boolean` or `{ background }`
					yandex: false, // Create Yandex browser icon. `boolean` or `{ background }`
				},
			}
		}),
		new WorkboxPlugin.GenerateSW({
			cacheId: 'webboard-sw-domain',
			swDest: 'service-worker.js',
			clientsClaim: true,
			skipWaiting: true,
			runtimeCaching: [
				{
					urlPattern: new RegExp('^https://fonts.(?:googleapis|gstatic).com/(.*)'),
					handler: 'cacheFirst'
				}
			]
		}),
	]
});
