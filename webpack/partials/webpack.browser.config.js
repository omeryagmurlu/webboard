const HtmlWebpackPlugin = require('html-webpack-plugin');
// const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const isCI = require('is-ci');
const webpackConfig = require('webpack-config');
const webpack = require('webpack');
const partials = require('./__partials');

const main = path.resolve(__dirname, '../..');
const Config = webpackConfig.Config;

module.exports = new Config().merge({
	output: {
		filename: 'browser-[name].js',
		chunkFilename: 'browser-[name].bundle.js',
		path: path.resolve(main, 'dist-browser'),
	},
	plugins: [
		!isCI && new CleanWebpackPlugin(['dist-browser'], { root: main, exclude: ['.noremove'] }),
		new webpack.DefinePlugin({
			__BROWSER__: JSON.stringify(true),
			__ELECTRON__: JSON.stringify(false),
			__MEDIUM__: JSON.stringify('browser'),
			__ENTRY__: JSON.stringify(false),
			...partials.commonDefines
		}),
		new HtmlWebpackPlugin({
			title: 'WebBoard',
			filename: 'index.html',
			template: 'index.base.html',
			appMountId: 'react-binding',
			runningIn: 'browser',
			chunksSortMode: 'none', // REVIEW: upstream fix,
			additionalCss: [
				// 'https://fonts.googleapis.com/css?family=Roboto:300,400,500&amp;subset=latin-ext',
				'https://fonts.googleapis.com/css?family=Raleway:300,400,500&amp;subset=latin-ext',
			]
		}),
		// new ScriptExtHtmlWebpackPlugin({
		// 	// prefetch: {
		// 	// 	test: [/actions/],
		// 	// 	chunks: 'async'
		// 	// }
		// 	// defaultAttribute: 'async'
		// }),
	].filter(v => v)
});
