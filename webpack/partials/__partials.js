const path = require('path');
const jsonImporter = require('node-sass-json-importer');

const devMode = process.env.NODE_ENV !== 'production';

exports.css = {
	test: /\.css$/,
	use: [{
		loader: 'css-loader',
		options: {
			camelCase: true
		}
	}]
};
exports.scss = {
	test: /\.scss$/,
	exclude: /(node_modules|bower_components)/,
	use: [{
		loader: 'css-loader',
		options: {
			modules: true,
			localIdentName: devMode ? '[path][name]__[local]--[hash:base64:5]' : '[hash:base64:5]',
			minimize: true,
			camelCase: true,
			importLoaders: 1
		}
	}, {
		loader: 'sass-loader',
		options: {
			includePaths: [],
			importer: jsonImporter
		}
	}]
};

exports.commonDefines = {
	__APP_ROOT__: JSON.stringify(path.resolve(__dirname, '../..'))
};
