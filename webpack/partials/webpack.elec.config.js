const HtmlWebpackPlugin = require('html-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const webpackConfig = require('webpack-config');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');
const isCI = require('is-ci');
const nodeExternals = require('webpack-node-externals');
const path = require('path');
const fs = require('fs');
const partials = require('./__partials');

const Config = webpackConfig.Config;
const main = path.resolve(__dirname, '../..');
const entries = {};

fs.readdirSync(path.resolve(main, 'entries')).forEach(file => { // child-renderer istenmio
	entries[path.parse(file).name] = path.resolve(main, 'entries', file);
});

// console.log(entries);

module.exports = new Config().merge({
	entry: entries,
	target: 'electron-renderer',
	externals: [nodeExternals({
		whitelist: ['app', /typeface/]
	})],
	output: {
		filename: 'electron-[name].js',
		chunkFilename: 'electron-[name].bundle.js',
		path: path.resolve(main, 'dist'),
	},
	plugins: [
		!isCI && new CleanWebpackPlugin(['dist'], { root: main, exclude: ['.noremove'] }),
		new webpack.DefinePlugin({
			__ELECTRON__: JSON.stringify(true),
			__BROWSER__: JSON.stringify(false),
			__MEDIUM__: JSON.stringify('electron'),
			__ENTRY__: JSON.stringify(false),
			...partials.commonDefines
		}),
		new HtmlWebpackPlugin({
			// inject: false,
			title: 'WebBoard',
			filename: 'index.electron.html',
			template: 'index.base.html',
			appMountId: 'react-binding',
			runningIn: 'electron',
			chunks: ['app', 'runtime~app', 'vendors~app'],
			chunksSortMode: 'none', // REVIEW: upstream fix
		}),
		new ScriptExtHtmlWebpackPlugin({ // REVIEW: UPSTREAM, this doesn't work with multiple HTML
			prefetch: {
				test: /[\s\S]*/,
				chunks: 'async'
			},
			chunks: ['app']
		}),
		...Object.keys(entries).map(eKey => new HtmlWebpackPlugin({
			// inject: false,
			title: eKey,
			filename: `index.electron.${eKey}.html`,
			template: 'index.base.html',
			appMountId: 'react-binding',
			runningIn: `electron:${eKey}`,
			chunks: [eKey, `runtime~${eKey}`, `vendors~${eKey}`],
			chunksSortMode: 'none',
		}))
	].filter(v => v)
});
