const webpackConfig = require('webpack-config');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack');
const partials = require('./__partials');

const Config = webpackConfig.Config;
const main = path.resolve(__dirname, '../..');

module.exports = new Config().merge({
	entry: {
		'server-view': [
			'./entries/server-view'
		]
	},
	resolve: {
		alias: {
			'app/utils/HOCs/contextHOC': path.resolve(main, 'app/utils/HOCs/contextHOCFake.js'),
			app: path.resolve(main, 'app'),
			appremote: path.resolve(main, 'entries/server-view'),
		}
	},
	externals: [],
	output: {
		path: path.resolve(main, 'dist/server-public'),
	},
	optimization: {
		splitChunks: {
			chunks: 'all'
		},
		minimize: true,
		runtimeChunk: true
	},
	plugins: [
		process.env.NODE_ENV === 'production' && new MiniCssExtractPlugin({
			// Options similar to the same options in webpackOptions.output
			// both options are optional
			filename: '[name].css',
			chunkFilename: '[id].css',
		}),
		process.env.NODE_ENV === 'dev' && new webpack.NamedModulesPlugin(),
		process.env.NODE_ENV === 'dev' && new webpack.HotModuleReplacementPlugin(),
		new webpack.DefinePlugin({
			__ELECTRON__: JSON.stringify(false),
			__BROWSER__: JSON.stringify(true),
			__MEDIUM__: JSON.stringify('server-view'), // cunku internette yancak
			__ENTRY__: JSON.stringify(true),
			__DEVELOPMENT__: JSON.stringify(process.env.NODE_ENV === 'dev'),
			__PRODUCTION__: JSON.stringify(process.env.NODE_ENV === 'production'),
			...partials.commonDefines
		}),
		new HtmlWebpackPlugin({
			title: 'server-view',
			filename: `index.electron.${'server-view'}.html`,
			template: 'index.base.html',
			appMountId: 'react-binding',
			runningIn: `electron:${'server-view'}`,
			chunks: ['server-view', 'runtime~server-view', 'vendors~server-view'],
			chunksSortMode: 'none',
		}),
	].filter(v => v),
	devServer: {},
	target: 'web'
});
