const path = require('path');
const webpackConfig = require('webpack-config');
const webpack = require('webpack');
const partials = require('./__partials');

const { Config } = webpackConfig;
const main = path.resolve(__dirname, '../..');

const prependStyleModule = rule => {
	rule.use.unshift({
		loader: 'style-loader'
	});
	return rule;
};

module.exports = new Config().merge({
	devtool: 'cheap-module-source-map',
	mode: 'development',
	module: {
		rules: [
			prependStyleModule(partials.css),
			prependStyleModule(partials.scss)
		]
	},
	optimization: {
		splitChunks: {
			chunks: 'all',
			minSize: 3000000
		},
		runtimeChunk: false
	},
	plugins: [
		new webpack.DefinePlugin({
			__DEVELOPMENT__: JSON.stringify(true),
			__PRODUCTION__: JSON.stringify(false),
			...partials.commonDefines
		}),
		new webpack.NamedModulesPlugin(),
		new webpack.HotModuleReplacementPlugin()
	],


	// output: {
	// 	publicPath: '/'
	// },
	devServer: {
		contentBase: path.resolve(main, 'dist-browser'),
		hot: true
	},
	watchOptions: {
		poll: 1000,
	}
});
