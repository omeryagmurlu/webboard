const webpackConfig = require('webpack-config');
const webpack = require('webpack');
const partials = require('./__partials');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const { Config } = webpackConfig;

module.exports = new Config().merge({
	mode: 'production',
	module: {
		rules: [
			{
				test: /\.css$/,
				use: [
					MiniCssExtractPlugin.loader,
					...partials.css.use
				],
			},
			{
				test: /\.scss$/,
				exclude: /(node_modules|bower_components)/,
				use: [
					MiniCssExtractPlugin.loader,
					...partials.scss.use
				],
			},
		]
	},
	optimization: {
		splitChunks: {
			chunks: 'all'
		},
		minimize: true,
		runtimeChunk: true
	},
	plugins: [
		new MiniCssExtractPlugin({
			// Options similar to the same options in webpackOptions.output
			// both options are optional
			filename: '[name].css',
			chunkFilename: '[id].css',
		}),
		new webpack.DefinePlugin({
			__PRODUCTION__: JSON.stringify(true),
			__DEVELOPMENT__: JSON.stringify(false),
			'process.env.NODE_ENV': JSON.stringify('production'),
			...partials.commonDefines
		}),
		new webpack.HashedModuleIdsPlugin(),
	]
});
