const webpackConfig = require('webpack-config');
const path = require('path');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const isCI = require('is-ci');

const { Config } = webpackConfig;

const main = path.resolve(__dirname, '../..');

module.exports = new Config().merge({
	context: main,
	entry: {
		app: [
			'./app/index.js'
		],
		// fonts: [ // FIXME
		// 	'./app/styles/fonts/fonts.scss'
		// ]
	},
	resolve: {
		alias: {
			app: path.resolve(main, 'app'),
			appremote: path.resolve(main, 'entries/server-view'),
		}
	},
	module: {
		rules: [
			{
				test: /(pdf.worker.min.js)/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name() {
								if (process.env.NODE_ENV === 'dev') {
									return '[name]_file-loader.[ext]';
								}
								return '[hash].[ext]';
							}
						}
					}
				]
			},
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							presets: ['react'].concat(process.env.TARGET === 'browser' ? [['env', { modules: false }]] : []),
							plugins: ['transform-class-properties', 'transform-object-rest-spread', 'syntax-dynamic-import']
						}
					}
					// 'eslint-loader'
				]
			},
			{
				test: /\.md$/,
				use: [
					{
						loader: 'html-loader'
					},
					{
						loader: 'markdown-loader'
					}
				]
			},
			{
				test: /\.(jpg|png|gif)$/,
				use: {
					loader: 'url-loader?limit=8192'
				}
			},
			{
				loader: 'url-loader',
				test: /\.(svg|eot|ttf|woff|woff2)?$/
			},
			{
				loader: 'raw-loader',
				test: /\.(txt)?$/
			},
		]
	},
	plugins: [
		// !isCI && new BundleAnalyzerPlugin({ analyzerMode: 'static', openAnalyzer: false }), // DO NOT LEAVE IT ON
		// !isCI && new WebpackMonitor({
		// 	capture: true,
		// 	// launch: true,
		// })
	].filter(v => v)
});
