const webpackConfig = require('webpack-config');
const servWebp = require('./partials/webpack.server-view.config');

const Config = webpackConfig.Config;

module.exports = [
	new Config().extend(
		'webpack/partials/webpack.base.config.js',
		'webpack/partials/webpack.prod.config.js',
		'webpack/partials/webpack.elec.config.js',
	),
	{
		...new Config().extend(
			'webpack/partials/webpack.base.config.js',
			'webpack/partials/webpack.prod.config.js',
			'webpack/partials/webpack.elec.config.js',
		),
		...servWebp
	}
];
