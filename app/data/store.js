import _mapValues from 'lodash/mapValues';
import Book from 'app/features/Book';
import update from 'immutability-helper';
import { resolveDeepProps } from 'app/utils';

const someBooks = [
	// new Book({
	// 	generationOptions: {
	// 		pdf: '/home/omer/Downloads/sponsorluk-dosyasi-2018.pdf'
	// 	}
	// }),
	// new Book({
	// 	isDynamic: false,
	// 	generationOptions: {
	// 		pdf: './samp2.pdf'
	// 	}
	// }),
	// new Book({
	// 	isDynamic: false,
	// 	name: 'Bos Adam',
	// 	generationOptions: {
	// 		fill: 6
	// 	}
	// })
];

const store = {
	books: {
		default: someBooks,
		reviver: booksJson => (booksJson ? booksJson.map(b => Book.revive(b)) : undefined),
		setter: (books, cb) => {
			const newOne = books.map(book => update(book, {
				pages: pages => pages.map(page =>
					(page ? page.toJSON() : null)
				),
				promise: { $set: null }
			}));
			resolveDeepProps(newOne, 3).then(cb);
		}
	},
	settings: {
		default: {
			connection: {
				maxConnections: 3,
				port: 26273,
			},
			fullscreen: true,
			theme: 'giant-goldfish',
			clutter: true,
		}
	}
};

export const defaults = _mapValues(store, val => val.default);
export const revivers = _mapValues(store, val => val.reviver || (v => v));
export const setters = _mapValues(store, val => val.setter || ((x, cb) => cb(x)));
