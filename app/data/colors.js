export const WHITE = '#FFFFFF';
export const BLACK = '#000000';

const colors = [
	BLACK,
	WHITE,
	'#FF0000',
	'#004080',
	'#008000',
	'#FFFF00',
	'#C87400',
	'#800040',
	'#008080',
	'#5F2D0A'
];

export default colors;
