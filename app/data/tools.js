import { Pencil, Eraser, Eyedropper, Text, Ellipse, Rectangle, Pan, Line, Polygon } from 'app/styles/svg';
import { defaultToolGray } from 'app/styles/variables.json';

const {
	pencilColor1,
	pencilColor2,
	rubberHeadColor,
	eyedropperColor1,
	shapesColor1
} = {
	pencilColor1: null,
	pencilColor2: null,
	rubberHeadColor: '#F5F5DC',
	eyedropperColor1: 'white',
	shapesColor1: '#323232'
};

const SATFIL = 'saturate(1.50) sepia(0.3) brightness(0.9)';

export const PENCIL = {
	name: 'Pencil',
	stroke: 5,
	primaryColor: true,
	image: {
		elem: Pencil,
		style: {
			transform: 'translateY(-50%) rotate(-90deg)',
			top: '50%',
			position: 'relative',
			marginLeft: '-10%',
			filter: SATFIL
		},
		fill: [pencilColor1, pencilColor2, rubberHeadColor, defaultToolGray],
		strokeWidth: '2'
	}
};

export const ERASER = {
	name: 'Eraser',
	stroke: 5,
	primaryColor: false,
	image: {
		elem: Eraser,
		style: {
			transform: 'translateY(-50%) scaleY(0.5) rotate(-180deg)',
			top: '50%',
			position: 'relative',
			marginLeft: '-10%',
			filter: SATFIL

		},
		fill: [rubberHeadColor, defaultToolGray]
	}
};

export const EYEDROPPER = {
	name: 'Eyedropper',
	primaryColor: true,
	image: {
		elem: Eyedropper,
		style: {
			transform: 'translateY(-50%) rotate(90deg)',
			top: '50%',
			position: 'relative',
			marginLeft: '-10%',
			// filter: `saturate(1.50) sepia(.3) brightness(0.9) drop-shadow(0 0 15px ${defaultToolGray}`,
			filter: SATFIL,
		},
		fill: [eyedropperColor1, defaultToolGray],
	}
};

export const TEXT = {
	name: 'Text',
	primaryColor: true,
	image: {
		elem: Text,
		style: {
			transform: 'translateY(-50%)',
			top: '50%',
			width: '75%',
			filter: SATFIL,
			position: 'relative',
		},
		fill: [defaultToolGray],
	}
};

export const ELLIPSE = {
	name: 'Ellipse',
	stroke: 5,
	primaryColor: true,
	secondaryColor: true,
	getImage: (primary, secondary) => ({
		elem: Ellipse,
		style: {
			transform: 'translateY(-50%)',
			top: '50%',
			position: 'relative',
			filter: `saturate(1.50) sepia(0.3) brightness(0.9) drop-shadow(0 0 10px ${secondary})`
		},
		fill: [shapesColor1],
	})
};

export const LINE = {
	name: 'Line',
	stroke: 5,
	primaryColor: true,
	image: {
		elem: Line,
		style: {
			transform: 'translateY(-50%)',
			top: '50%',
			filter: SATFIL,
			position: 'relative',
		},
		fill: [shapesColor1],
		strokeWidth: '10'
	}
};

export const PAN = {
	name: 'Pan',
	image: {
		elem: Pan,
		style: {
			transform: 'translateY(-50%)',
			top: '50%',
			width: '50%',
			position: 'relative',
			filter: SATFIL,
			float: 'right'
		},
		fill: [defaultToolGray],
	}
};

export const POLYGON = {
	name: 'Polygon',
	stroke: 5,
	primaryColor: true,
	secondaryColor: true,
	getImage: (primary, secondary) => ({
		elem: Polygon,
		style: {
			transform: 'translateY(-50%)',
			width: '50%',
			top: '50%',
			position: 'relative',
			filter: `saturate(1.50) sepia(0.3) brightness(0.9) drop-shadow(0 0 10px ${secondary})`,
		},
		fill: [shapesColor1],
	})
};

export const RECTANGLE = {
	name: 'Rectangle',
	stroke: 5,
	primaryColor: true,
	secondaryColor: true,
	getImage: (primary, secondary) => ({
		elem: Rectangle,
		style: {
			transform: 'translateY(-50%)',
			width: '75%',
			top: '50%',
			position: 'relative',
			filter: `saturate(1.50) sepia(0.3) brightness(0.9) drop-shadow(0 0 10px ${secondary})`,
		},
		fill: [shapesColor1],
	})
};

export const toolsArray = [
	PENCIL,
	ERASER,
	TEXT,
	EYEDROPPER,
	LINE,
	PAN,
	ELLIPSE,
	POLYGON,
	RECTANGLE,
];

export const toolsObject = toolsArray.reduce((acc, curr) => {
	acc[curr.name] = curr;
	return acc;
}, {});

export default toolsArray;
