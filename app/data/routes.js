export default {
	'/board/:book': {
		comp: () => import(/* webpackChunkName: BoardContainer, webpackPrefetch: true */'app/containers/BoardContainer'),
		fullscreen: true,
	},
	'/books': () => import(/* webpackChunkName: BooksContainer, webpackPrefetch: true */'app/containers/BooksContainer'),
	'/': () => import(/* webpackChunkName: BooksContainer, webpackPrefetch: true */'app/containers/BooksContainer'),
};
