// import _mapValues from 'lodash/mapValues';
// import _isFunction from 'lodash/isFunction';
// import _isObject from 'lodash/isObject';
// import { ipcRenderer as ipc, remote as remElec } from 'electron';
//
// const guid = () =>
// 	Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
//
// const remoteFunction = (fn, namespace) => {
// 	if (!fn.name) {
// 		throw new Error(`To use a remoteFunction, you need to have named functions ${fn}`);
// 	}
// 	return {
// 		name: fn.name,
// 		JSON_REMOTE_FN: true,
// 		namespace
// 	};
// };
//
// const isRemoteFunction = (remote) => _isObject(remote) && remote.JSON_REMOTE_FN;
//
// const makeTransferCompliant = (obj, converts = [], nmspc) =>
// 	_mapValues(obj, val => {
// 		if (_isFunction(val)) {
// 			const f = remoteFunction(val, nmspc);
// 			converts.push({
// 				original: val,
// 				remote: f,
// 			});
// 			return f;
// 		}
// 		if (_isObject(val)) {
// 			return makeTransferCompliant(val, converts, nmspc);
// 		}
// 		return val;
// 	});
//
// export const createRemoteContext = (obj) => {
// 	const listeners = [];
// 	const converted = [];
// 	const nmspc = guid();
// 	const goodFnProps = makeTransferCompliant(obj, converted, nmspc);
// 	converted.forEach(({ original, remote }) => {
// 		const channel = `procedure:${remote.name}+${nmspc}`;
// 		const listener = async (event, respId, winName, ...args) => {
// 			// console.log(`called ${remote.name} with ${args} and ${respId}`);
// 			let retVal;
// 			try {
// 				retVal = await original(...args).catch(e => e);
// 			} catch (e) {
// 				retVal = e;
// 			}
// 			remElec.getGlobal('childWindows')[winName].webContents.send(respId, retVal);
// 		};
// 		ipc.on(channel, listener);
// 		listeners.push({ channel, listener });
// 	});
// 	const detachRemotes = () => listeners.forEach(({ channel, listener }) => {
// 		ipc.removeListener(channel, listener);
// 	});
//
// 	return {
// 		object: goodFnProps,
// 		detach: detachRemotes,
// 	};
// };
//
// export const callRemote = (winName, send, remote, ...args) => {
// 	if (!remote.JSON_REMOTE_FN) {
// 		throw new Error('callRemote can only call REMOTE FNs');
// 	}
// 	return new Promise((resolve, reject) => {
// 		const responseId = guid();
// 		ipc.once(responseId, (event, ret) => {
// 			console.log('me');
// 			resolve(ret);
// 		});
// 		send(`procedure:${remote.name}+${remote.namespace}`, responseId, winName, ...args);
// 	});
// };
//
// export const matchRemoteToLocalFun = (obj, whatTaDo = () => {}) =>
// 	_mapValues(obj, val => {
// 		if (isRemoteFunction(val)) {
// 			const f = (...argArr) => whatTaDo(val, argArr);
// 			return f;
// 		}
// 		if (_isObject(val)) {
// 			return matchRemoteToLocalFun(val);
// 		}
// 		return val;
// 	});
