import React from 'react';
import path from 'path';
import map from 'lodash/map';
import chroma from 'chroma-js';
import mapValues from 'lodash/mapValues';
import _isFunction from 'lodash/isFunction';
import promiseProps from 'promise-props';

export const bookInfoTexter = book => ([].concat(...[
	`${book.info.totalPage} pages`,
	`${!book.info.dynamic ? 'not ' : ''}dynamic`,
	book.info.origin && `from ${book.info.origin}`
].filter(v => v)
	.map(v => [v, INTERPUNKT(v)])).slice(0, -1));

export const toolIconProcess = (tool, curCols) => {
	const {
		elem: Icon, style: iconStyle, fill, ...svgProps
	} = tool.image || (tool.getImage && tool.getImage(curCols.primary, curCols.secondary)) || {};
	const fills = Icon && fill.reduce((acc, cur, i) => {
		let col = cur;
		if (i === 0) {
			col = col || curCols.primary;
		}
		if (i === 1) {
			col = col || chroma(curCols.primary).saturate(1.50).hex();
		}
		acc[`fill${i + 1}`] = col;
		return acc;
	}, {});

	return { fills, svgProps, Icon, iconStyle };
};

export const INTERPUNKT = v => <span key={v}>&nbsp;&nbsp;•&nbsp;&nbsp;</span>;
export const INTERPUNKT_TEXT = '  •  ';

export const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

export function isFullScreen() {
	return !((document.fullScreenElement !== undefined && document.fullScreenElement === null)
		|| (document.msFullscreenElement !== undefined && document.msFullscreenElement === null)
		|| (document.mozFullScreen !== undefined && !document.mozFullScreen)
		|| (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen));
}

export function exitFullScreen() {
	if (isFullScreen()) {
		if (document.cancelFullScreen) {
			document.cancelFullScreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen();
		} else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		}
	}
}

export function enterFullScreen() {
	const elem = document.documentElement;
	if (!isFullScreen()) {
		if (elem.requestFullScreen) {
			elem.requestFullScreen();
		} else if (elem.mozRequestFullScreen) {
			elem.mozRequestFullScreen();
		} else if (elem.webkitRequestFullScreen) {
			elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
		} else if (elem.msRequestFullscreen) {
			elem.msRequestFullscreen();
		}
	}
}

export function toggleFullScreen() {
	if (!isFullScreen()) {
		enterFullScreen();
	} else {
		exitFullScreen();
	}
}

export const setStateConnHook = (that, message) => (x, sendAll = false) => {
	let obj = x;
	if (_isFunction(x)) {
		obj = obj(that().state);
	}
	const cb = () => {
		(that().props.connection || that().props.app.connection)
			.sendToControllers('remote', message, JSON.parse(JSON.stringify(sendAll ? that().state : obj)));
	};
	if (!obj) {
		return that().forceUpdate(cb);
	}
	return that().setState(obj, cb);
};

export const guid = () =>
	Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

export const stylist = (style) => {
	if (!style) {
		throw new ReferenceError(`stylist: unknown style ${style}`);
	}
	const fun = (className, theme) => {
		if (Array.isArray(className)) {
			return className.filter(v => v).map(name => fun(name, theme)).join(' ');
		}
		const retVal = [
			style[className],
			style[`${className}-${theme}`]
		].filter(v => v);
		if (retVal.length === 0) {
			throw new ReferenceError(`stylist: could'nt find: ${className} in [${Object.keys(style).join(' ,')}]`);
		}
		return retVal.join(' ');
	};

	return fun;
};

export const getFilename = (fp) => path.basename(fp, path.extname(fp));

export const noop = () => {
	if (__DEVELOPMENT__) {
		// console.warn('You have called the NOOP');
	}
};

export const preload = Comp => ({
	onMouseEnter: Comp.preload,
	onFocus: Comp.preload,
});

export function delay(ms) {
	let ctr;
	let rej;
	const p = new Promise((resolve, reject) => {
		ctr = setTimeout(resolve, ms);
		rej = reject;
	});
	p.cancel = () => {
		clearTimeout(ctr);
		rej(new Error('Delay Cancelled'));
	};
	return p;
}

// https://stackoverflow.com/questions/5775469/whats-the-valid-way-to-include-an-image-with-no-src/5775531#5775531
export const BLANK_IMAGE = 'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=';

function resolveArrayOrObject(obj) {
	return Promise.resolve(obj).then((resolved) =>
		(Array.isArray(resolved) ? Promise.all(resolved) : promiseProps(resolved))
	);
}

function isPrimitive(val) {
	return val == null || val === true || val === false ||
		typeof val === 'string' || typeof val === 'number';
}

export function resolveDeepProps(obj, limit = Infinity, nest = 0) {
	if (isPrimitive(obj) || nest > limit) {
		return Promise.resolve(obj);
	}
	return resolveArrayOrObject(obj)
		.then((resolved) => {
			const mapfun = Array.isArray(resolved) ? map : mapValues;
			return resolveArrayOrObject(mapfun(resolved, (x) => resolveDeepProps(x, limit, nest + 1)));
		});
}

export const onMouseOver = fn => ({
	onMouseOver: (...x) => fn(...x),
	onFocus: (...x) => fn(...x),
});
