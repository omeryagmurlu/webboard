import React from 'react';
import PropTypes from 'prop-types';
import { stylist } from 'app/utils';
import style from './style.scss';

const css = stylist(style);

// Bu HOC biraz karisik, selected classina disaridan erisim olmadigindan
// onun degistirecegi seylerin ayarlamasi inline yapiliyor, ek css kesmiyor
// cunku. Ama hover ve focus stateleri sadece css de var, onlarin bulundugu
// class da disaridan aliniyor bu yuzden, inline degil yani

// Evet :D, biraz karisik
const threeSwipe = (
	{ selectClass = css('selected'), normalClass = css('normal'), lean = '' }, // opts
	[selectedProp = 'threeSwipeHOCselected'] // needed props
) => (Wrapped) => {
	const ThreeSwipeHOC = ({
		[selectedProp]: selected,
		...props
	}) => (
		<div
			className={`${css([
				lean, 'default-th'
			])} ${selected && selectClass} ${normalClass}`}
		>
			<Wrapped {...props} />
		</div>
	);

	ThreeSwipeHOC.defaultProps = {
		[selectedProp]: false
	};

	ThreeSwipeHOC.propTypes = {
		[selectedProp]: PropTypes.bool
	};

	return ThreeSwipeHOC;
};

export default threeSwipe;
