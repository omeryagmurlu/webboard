import { compose, withState, withHandlers } from 'recompose';
import { guid } from 'app/utils';

export const stateHOC = (initialState, stateKey = 'state', setStateKey = 'setState') => {
	const setStateProp = `${guid()}+setStateProp`;
	return compose(
		withState(stateKey, setStateProp, initialState),
		withHandlers({
			[setStateKey]: ({ [setStateProp]: setState, [stateKey]: state }) =>
				(patch, cb = () => {}) => {
					const newState = { ...state, ...patch };
					setState(newState, () => cb(newState));
				},
		})
	);
};

export const amqdgds = 57;
