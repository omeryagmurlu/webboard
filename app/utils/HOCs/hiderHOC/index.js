import React from 'react';
import PropTypes from 'prop-types';
import { stylist } from 'app/utils';
import style from './style.scss';

const css = stylist(style);

// options, needed props, supplied props
const hider = (
	{ hideRight, hideLeft },
	[hiddenProp = 'hiderHOChidden']
) => (Wrapped) => {
	const HiderHOC = ({
		[hiddenProp]: hidden,
		...props
	}) => (
		<div className={css(['hider', hidden ? 'hidden' : '', hideLeft && 'left', hideRight && 'right'])}>
			<Wrapped {...props} />
		</div>
	);

	HiderHOC.propTypes = {
		[hiddenProp]: PropTypes.bool.isRequired
	};

	return HiderHOC;
};

// eger bu hoc render icinde lazimsa ceken dosyanin en ustunde kucuk bir def yap
// orda mesela RightHider olustur fln

export default hider;
