import React from 'react';
import _omit from 'lodash/omit';
import { noop } from 'app/utils';

const targetEvent = (Wrap, targetProp = 'value', handlerName = 'onChange') => props => {
	const ourUp = 'onChange';

	const givenUpdater = props[ourUp] || noop;
	const handleEvent = e => givenUpdater(e.target[targetProp]);
	return (
		<Wrap {...({
			..._omit(props, ourUp),
			[handlerName]: handleEvent
		})}
		/>
	);
};

export default targetEvent;
