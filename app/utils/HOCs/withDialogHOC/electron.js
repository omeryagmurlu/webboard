import { ipcRenderer as ipc } from 'electron';
import { createRemoteContext } from 'app/utils/rpc';

// we don't send component, we send file name and other
// BrowserWindow finds file with that name
export class Modal {
	constructor({
		identifier,
		onClose,
		modalFileName,
	}) {
		this.identifier = identifier;
		this.onClose = onClose;
		this.modalFileName = modalFileName;

		this.wasOpened = false;
	}

	emulateRender({
		open,
		componentProps,
	}) {
		if (open === this.wasOpened) {
			return;
		}


		if (!open) {
			ipc.send('modal:close', this.identifier);
			return;
		}

		if (open) {
			const { object: fnedProps, detach } = createRemoteContext(componentProps);

			ipc.send('modal:open', this.identifier, this.modalFileName, fnedProps);
			this.wasOpened = open;

			ipc.once(`Re:modal:arg(${this.identifier}):close`, () => {
				this.wasOpened = !open;
				detach();
				this.onClose();
			});
		}
	}
}

export const amp = 5;
