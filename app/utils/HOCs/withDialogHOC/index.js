import React, { Component, Fragment } from 'react';
import Dialog from '@material-ui/core/Dialog';

// !! Electron native dialog is disabled!, it is not worth the hassle

// modalFileName is for Electron, it spawns a new BrowserWindow (child-renderers/dialog)
// modalFileName should be one of the keys in modals var of that file
const withDialog = (DialogInner, modalFileName, {
	dialogProps = {},
	moreInnerProps = {},
	propName,
}) => (Wrap) => class withDialogHOC extends Component {
	static displayName = `WithDialog(${Wrap.name}): ${propName}`

	constructor(props) {
		super(props);

		this.state = {
			open: false
		};

		// if (__ELECTRON__) {
		// 	import(/* webpackChunkName: withDialogElectron */ './electron').then(({ Modal }) => {
		// 		this.elecModal = new Modal({
		// 			identifier: propName,
		// 			modalFileName,
		// 			onClose: this.closeDialog,
		// 		});
		// 	});
		// }
		Object.defineProperty(this.closeDialog, 'name', { value: `${propName}Close` });
		Object.defineProperty(this.openDialog, 'name', { value: `${propName}Open` });
		Object.defineProperty(this.toggleDialog, 'name', { value: `${propName}Toggle` });
	}

	openDialog = () => this.setState({ open: true })
	closeDialog = () => this.setState({ open: false })
	toggleDialog = () => this.setState(prev => ({ open: !prev.open }))

	render() {
		// Both the wrapped component, as well as the dialog get the props
		// passed to this, since they are together
		const wrappedProps = {
			...this.props,
			...{
				[`${propName}Open`]: this.openDialog,
				[`${propName}Close`]: this.closeDialog,
				[`${propName}Toggle`]: this.toggleDialog,
			}
		};

		// if (__ELECTRON__) {
		// 	if (this.elecModal) {
		// 		this.elecModal.emulateRender({
		// 			open: this.state.open,
		// 			componentProps: {
		// 				...wrappedProps,
		// 				...moreInnerProps
		// 			},
		// 		});
		// 	}
		// 	return (
		// 		<Wrap
		// 			{...wrappedProps}
		// 		/>
		// 	);
		// }

		return (
			<Fragment>
				<Dialog
					onClose={this.closeDialog}
					open={this.state.open}
					{...dialogProps}
				>
					<DialogInner
						{...wrappedProps}
						{...moreInnerProps}
					/>
				</Dialog>
				<Wrap
					{...wrappedProps}
				/>
			</Fragment>
		);
	}
};

export default withDialog;
