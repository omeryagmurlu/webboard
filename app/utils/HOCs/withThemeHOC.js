import React from 'react';
import { stylist } from 'app/utils';
import withFromApp from './withFromAppHOC';

const withTheme = (toStylist) => (Wrap) => withFromApp('theme')(({ theme, ...remProps }) => {
	const css = stylist(toStylist);

	const cusCss = (className) => css(className, theme);
	return (
		<Wrap
			{...remProps}
			css={cusCss}
		/>
	);
});

export default withTheme;
