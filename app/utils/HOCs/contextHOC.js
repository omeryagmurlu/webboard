import React from 'react';
import { AppContext } from 'app/containers/AppContainer';

const contextify = (Wrap) => (props) => (
	<AppContext.Consumer>
		{app => <Wrap {...props} app={app} />}
	</AppContext.Consumer>
);

export default contextify;
