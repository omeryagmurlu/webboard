import React from 'react';
import contextify from 'app/utils/HOCs/contextHOC';

const withFromApp = (propName) => (Wrap) => contextify(({ app, ...remProps }) => {
	let props = propName;
	if (!Array.isArray(propName)) {
		props = [propName];
	}
	props = props.reduce((acc, key) => {
		acc[key] = app[key];
		return acc;
	}, {});
	return (
		<Wrap {...remProps} {...props} />
	);
});

export default withFromApp;
