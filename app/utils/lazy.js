import React from 'react';
import Loadable from 'react-loadable';
import SimpleLoader from 'app/components/Loader/SimpleLoader';

const fnfc = (Comp) => (par) => {
	const obj = typeof par === 'function' ? { loader: par } : par;
	return Loadable({
		loading: Comp,
		...obj
	});
};

export const simpleLazy = fnfc(SimpleLoader);
