import React from 'react';
import PropTypes from 'prop-types';

const Divton = ({
	onClick, tabIndex, children, ...props
}) => (
	<div
		onClick={() => onClick()}
		onKeyPress={(e) => e.key === 'Enter' && onClick()}
		role="button"
		tabIndex={tabIndex}
		{...props}
	>
		{children}
	</div>
);

Divton.defaultProps = {
	tabIndex: 0,
	children: []
};

Divton.propTypes = {
	onClick: PropTypes.func.isRequired,
	tabIndex: PropTypes.number,
	children: PropTypes.any
};

export default Divton;
