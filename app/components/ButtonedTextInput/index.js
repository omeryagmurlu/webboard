import React from 'react';
import PropTypes from 'prop-types';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import { withState } from 'recompose';

import Send from '@material-ui/icons/Send';

const ButtonedTextInput = ({
	currVal, setCurrVal,
	side, label, icon: Icon, component: Component, autoReset, errText,
	onClick, validator, value, // value sadece asagi gitmesin diye
	...pass
}) => (
	<Component
		label={label}
		value={currVal}
		onChange={e => {
			setCurrVal(e.target.value);
		}}
		helperText={!validator(currVal) ? errText : ' '}
		error={!validator(currVal)}
		InputProps={{
			[`${side}Adornment`]: (
				<InputAdornment position={side}>
					<IconButton
						onClick={() => {
							if (validator(currVal)) {
								onClick(currVal);
								if (autoReset) setCurrVal(value);
							}
						}}
						disabled={!validator(currVal)}
					>
						<Icon />
					</IconButton>
				</InputAdornment>
			),
		}}
		{...pass}
	/>
);

ButtonedTextInput.defaultProps = {
	validator: () => true,
	errText: 'error',

	autoReset: false,
	label: '',
	side: 'end',
	icon: Send,
	value: '',
	component: TextField,
};

ButtonedTextInput.propTypes = {
	onClick: PropTypes.func.isRequired,
	validator: PropTypes.func,
	errText: PropTypes.string,
	autoReset: PropTypes.bool,

	label: PropTypes.string,
	side: PropTypes.string,
	icon: PropTypes.func,
	component: PropTypes.func,

	value: PropTypes.any, // used in initialVal

	currVal: PropTypes.any.isRequired,
	setCurrVal: PropTypes.func.isRequired,
};

export default withState('currVal', 'setCurrVal', ({ value = '' }) => value)(ButtonedTextInput);
