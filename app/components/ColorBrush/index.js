import React, { Fragment } from 'react';
import hider from 'app/utils/HOCs/hiderHOC';
import threeSwipeHOC from 'app/utils/HOCs/threeSwipeHOC';
import _flow from 'lodash/flow';
import Divton from 'app/components/Divton';
import { stylist } from 'app/utils';
import { BLACK, WHITE } from 'app/data/colors';
import { PaintBrush, CuteBucket } from 'app/styles/svg';

import commonStyle from 'app/styles/common.scss';
import style from './style.scss';

const css = stylist({ ...commonStyle, ...style });

const contrastWandB = color => (color === BLACK ? WHITE : color === WHITE ? BLACK : color); // eslint-disable-line

const ColorBrush = ({
	color, currSecColor, onPrimary, onSecondary
}) => (
	<Fragment>
		<PaintBrush
			className={css(['icon-svg', 'paint-brush'])}
			width="75%"
			fill1={color}
			style={{
				filter: `saturate(1.50) sepia(0.3) brightness(0.9) drop-shadow(0 0 15px ${contrastWandB(color)})`
			}}
		/>
		<Divton
			className={css('primary-brush')}
			onClick={onPrimary}
			aria-label={`primary color ${color}`}
		/>
		{currSecColor &&
			<Divton
				className={css('secondary-brush')}
				onClick={onSecondary}
				aria-label={`secondary color ${currSecColor}`}
			>
				<CuteBucket
					className={css(['icon-svg', 'cute-bucket'])}
					fill1={currSecColor}
				/>
			</Divton>
		}
	</Fragment>
);

export default _flow([
	hider({
		hideRight: true
	}, ['hidden']),
	threeSwipeHOC({
		selectClass: css('three-swipe-selected'),
		normalClass: css('three-swipe-initial')
	}, ['selected'])
])(ColorBrush);
