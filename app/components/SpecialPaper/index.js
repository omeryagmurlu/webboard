import React from 'react';
import Paper from '@material-ui/core/Paper';

import withTheme from 'app/utils/HOCs/withThemeHOC';
import style from './style.scss';

const SpecialPaper = ({ css, className, ...pass }) => (
	<Paper className={`${className} ${css('special-paper')}`} {...pass} />
);

export default withTheme(style)(SpecialPaper);
