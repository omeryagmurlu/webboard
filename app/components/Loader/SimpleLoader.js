import React from 'react';

const SimpleLoader = ({ pastDelay }) => (pastDelay ? (<div>Loading...</div>) : null);

export default SimpleLoader;
