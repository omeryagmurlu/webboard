import React from 'react';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import isFunction from 'lodash/isFunction';

import { noop } from 'app/utils';

const FileButton = ({
	onChange,
	ext,
	limit,
	multiple,
	component: Comp,
	children,
	...pass
}) => {
	const clickHandler = () => {
		const input = document.createElement('input');
		input.type = 'file';
		if (multiple) input.multiple = true;
		if (ext && limit) input.accept = `.${ext}`;
		const fn = () => {
			input.removeEventListener('change', fn);
			const norm = FileButton.normalizePath([...input.files]);
			if (multiple) {
				if (!isFunction(multiple)) return onChange(norm);
				return multiple(norm);
			}
			return onChange(norm[0]);
		};
		input.addEventListener('change', fn, false);
		input.click();
	};

	return (
		<Comp
			onClick={clickHandler}
			{...pass}
		>
			{children}
		</Comp>
	);
};

FileButton.normalizePath = (files) => files.map(v => (
	__ELECTRON__
		? v.path
		: URL.createObjectURL(v)
));

FileButton.defaultProps = {
	onChange: noop,
	ext: undefined,
	limit: true,
	multiple: false,
	component: IconButton,
	children: undefined,
};

FileButton.propTypes = {
	onChange: PropTypes.func,
	ext: PropTypes.string,
	limit: PropTypes.bool,
	multiple: PropTypes.oneOfType([
		PropTypes.bool,
		PropTypes.func
	]),
	component: PropTypes.func,
	children: PropTypes.any,
};

export default FileButton;
