import React from 'react';
import PropTypes from 'prop-types';
import { onlyUpdateForKeys } from 'recompose';
import CardMedia from '@material-ui/core/CardMedia';
import Paper from '@material-ui/core/Paper';

import EmptyStateText from 'app/components/EmptyStateText';

import { stylist, BLANK_IMAGE } from 'app/utils';

import style from './style.scss';

const css = stylist({ ...style });

const ImageFrame = ({
	img,
	className,
	...pass
}) => (
	<Paper className={`${className} ${css('resim-paper')}`}>
		<EmptyStateText
			primary="No preview"
			isEmpty={img === BLANK_IMAGE}
		/>
		<CardMedia
			className={css('resim')}
			image={img}
			{...pass}
		/>
	</Paper>
);

ImageFrame.defaultProps = {
	img: BLANK_IMAGE,
	className: '',
};

ImageFrame.propTypes = {
	img: PropTypes.any,
	className: PropTypes.string,
};

export default onlyUpdateForKeys(['img', 'title'])(ImageFrame);
