import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';

import { stylist } from 'app/utils';

import style from './style.scss';

const css = stylist({ ...style });

const EmptyStateText = ({
	primary,
	secondary,
	isEmpty,
}) => (!isEmpty ? null : (
	<div className={css('cont')}>
		<Typography variant="display1">{primary}</Typography>
		{secondary && <Typography variant="subheading">{secondary}</Typography>}
	</div>
));

EmptyStateText.defaultProps = {
	isEmpty: true,
	secondary: '',
};

EmptyStateText.propTypes = {
	primary: PropTypes.string.isRequired,
	secondary: PropTypes.string,
	isEmpty: PropTypes.bool,
};

export default EmptyStateText;
