import React from 'react';
import PropTypes from 'prop-types';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

const LabeledCheckbox = ({ label, value, onChange, ...pass }) => (
	<FormControlLabel
		control={
			<Checkbox
				value={label}
				checked={value}
				onChange={onChange}
			/>
		}
		label={label}
		{...pass}
	/>
);

LabeledCheckbox.propTypes = {
	label: PropTypes.string.isRequired,
	value: PropTypes.bool.isRequired,
	onChange: PropTypes.func.isRequired,
};

export default LabeledCheckbox;
