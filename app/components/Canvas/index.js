import React, { Component } from 'react';
import PropTypes from 'prop-types';
import chroma from 'chroma-js';
import _omit from 'lodash/omit';
import literally from 'literallycanvas/lib/js/literallycanvas-core.js';
import 'app/../node_modules/literallycanvas/lib/css/literallycanvas.css';
import withTheme from 'app/utils/HOCs/withThemeHOC';
import { BLANK_IMAGE } from 'app/utils';
import { PropTypesCols } from 'app/prop-schema';
import { toolsArray } from 'app/data/tools';

import style from './style.scss';

const BGImage = ({ image = BLANK_IMAGE, lc: lcfirst } = {}) => {
	let lc = lcfirst;
	const img = new Image();
	img.crossorigin = 'anonymous';
	img.src = image;

	const change = (newImageSrc) => {
		Promise.resolve(newImageSrc).then(src => {
			img.src = src || '';
			setTimeout(() => lc.pan(0, 0), 0);
		});
	};

	const setLC = (newlc) => (lc = newlc);
	const getImage = () => img;

	return ({ change, getImage, setLC });
};

class Canvas extends Component {
	constructor(props) {
		super(props);
		this.LCtools = {};
		this.eventsToDetach = [];
	}

	componentDidMount() {
		this.bg = BGImage();
		this.lc = literally.init(this.element, {
			imageSize: { width: null, height: null },
			backgroundShapes: [
				literally.createShape('Image', { x: 0, y: 0, image: this.bg.getImage() })
			]
		});
		this.bg.setLC(this.lc);

		toolsArray
			.filter(tool => literally.tools[tool.name])
			.forEach(tool => (this.LCtools[tool.name] = new literally.tools[tool.name](this.lc)));

		this.attachEvents();
		this.adaptToProps(this.props, {});
	}

	// HOLY FUCKING SHIT BURADA ARTIK SIDE EFFECT OLMASIN DIYOLLA REACT 17
	componentWillReceiveProps(newProps) {
		this.adaptToProps(newProps);
	}

	componentWillUnmount() {
		this.detachEvents();
		this.lc.teardown();
	}

	adaptToProps({
		toolName, toolStroke, colors, canvasRestore, canvasImg
	}, oldPropsOptional) {
		const oldProps = oldPropsOptional || this.props;
		if (canvasRestore !== oldProps.canvasRestore) {
			// yes shallow it is intended
			this.lc.clear();
			this.lc.loadSnapshot(canvasRestore);
		}

		Promise.all([canvasImg, oldProps.canvasImg]).then(([bir, iki]) => {
			if (bir !== iki) {
				this.bg.change(canvasImg);
			}
		});

		if (toolName !== oldProps.toolName) {
			this.lc.setTool(this.LCtools[toolName]);
		}

		if (toolStroke && toolStroke !== oldProps.toolStroke) {
			this.lc.trigger('setStrokeWidth', toolStroke);
		}

		Object.keys(colors).forEach(type => {
			if (oldProps.colors && colors[type] !== oldProps.colors[type]) {
				this.lc.setColor(type, colors[type]);
			}
		});
	}

	attachEvents() {
		['primary', 'secondary'].forEach(type => {
			this.eventsToDetach.push(this.lc
				.on(`${type}ColorChange`, (newColor) => newColor && this.props.changeColor(chroma(newColor).css(), type)));
		});

		const changeHandler = () => {
			const lastestSnap = this.lc.getSnapshot(['shapes', 'imageSize', 'position', 'backgroundShapes']);
			const { width: dW, height: dH } = this.lc.getDefaultImageRect();

			const getImg = () => {
				const img = literally.renderSnapshotToImage(lastestSnap, {
					rect: {
						x: 0,
						y: 0,
						width: Math.max(dW, window.innerWidth),
						height: Math.max(dH, window.innerHeight),
					}
				});
				if (img) {
					return img.toDataURL();
				}
				return undefined;
			};

			return ({
				save: _omit(lastestSnap, ['backgroundShapes']),
				preview: getImg
			});

			// this.props.updateSavedCanvas({
			// 	save: _omit(lastestSnap, ['backgroundShapes']),
			// 	preview: getImg
			// });
		};
		this.props.setCanvDataReq(changeHandler);
		// this.eventsToDetach.push(this.lc // we not keeping backgroundShapes, since load it seperate
		// 	.on('drawingChange', changeHandler));
	}

	detachEvents() {
		this.eventsToDetach.forEach(unsubscribe => unsubscribe());
		this.eventsToDetach = [];
	}

	render() {
		const { css } = this.props;
		return (
			<div
				className={css('style')}
				ref={el => (this.element = el)}
			/>
		);
	}
}

Canvas.defaultProps = {
	canvasImg: '',
	canvasRestore: undefined,
	toolStroke: undefined
};

Canvas.propTypes = {
	toolName: PropTypes.string.isRequired,
	toolStroke: PropTypes.number,
	colors: PropTypesCols.isRequired,
	canvasImg: PropTypes.any,

	changeColor: PropTypes.func.isRequired,

	canvasRestore: PropTypes.any,
	setCanvDataReq: PropTypes.func.isRequired,

	css: PropTypes.any.isRequired,
};

export default withTheme(style)(Canvas);
