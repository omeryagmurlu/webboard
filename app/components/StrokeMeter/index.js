import React from 'react';
import PropTypes from 'prop-types';
import { compose, onlyUpdateForKeys } from 'recompose';
import Divton from 'app/components/Divton';
import { stylist, noop } from 'app/utils';
import hider from 'app/utils/HOCs/hiderHOC';
import { Ruler } from 'app/styles/svg';

import commonStyle from 'app/styles/common.scss';
import style from './style.scss';

const css = stylist({ ...commonStyle, ...style });

const WIDTH = 200;

const StrokeMeter = ({
	onClick: changeStroke,
	currStroke,
	strokeSteps,
	className
}) => (
	<div
		className={`${css('stroked')} ${className}`}
	>
		<Ruler
			className={css(['icon-svg', 'ruler'])}
			fill1="#2f2f2f"
		/>
		{typeof currStroke !== 'undefined'
			? Array(strokeSteps).fill(1).map((u, v) => { // eger saklandiksak checki
				const stroke = (((v) * (WIDTH / strokeSteps)) + 1);
				return (
					<Divton
						key={stroke}
						aria-label={`stroke width ${stroke}`}
						className={css([
							'pianotiles',
							currStroke === stroke ? 'selected' : ''
						])}
						onClick={() => changeStroke(stroke)}
						tabIndex={-10}
						style={{
							flexGrow: strokeSteps - v
						}}
					/>
				);
			})
			: undefined}
	</div>
);

StrokeMeter.defaultProps = {
	onClick: noop,
	strokeSteps: 60,
	currStroke: undefined,
	className: ''
};

StrokeMeter.propTypes = {
	onClick: PropTypes.func,
	currStroke: PropTypes.number,
	strokeSteps: PropTypes.number,
	className: PropTypes.string
};

export default compose(
	hider({
		hideRight: true
	}, ['hidden']),
	onlyUpdateForKeys(['currStroke', 'className'])
)(StrokeMeter);
