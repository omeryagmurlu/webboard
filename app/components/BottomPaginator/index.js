import React, { Fragment } from 'react';
import { func, number, oneOfType, bool } from 'prop-types';
import { onlyUpdateForKeys } from 'recompose';
import IconButton from '@material-ui/core/IconButton';
import ArrowBack from '@material-ui/icons/ArrowBack';
import ArrowForward from '@material-ui/icons/ArrowForward';
import NoteAdd from '@material-ui/icons/NoteAdd';

const BottomPaginator = ({ changePage, addNewPage, currPage, totalPage }) => (
	<Fragment>
		<IconButton aria-label="previous page" onClick={() => changePage(currPage - 1)} >
			<ArrowBack />
		</IconButton>
		<span>{` ${currPage} / ${totalPage} `}</span>
		<IconButton aria-label="previous page" onClick={() => changePage(currPage + 1)} >
			<ArrowForward />
		</IconButton>
		<IconButton aria-label="insert page" disabled={!addNewPage} onClick={() => addNewPage()} >
			<NoteAdd />
		</IconButton>
	</Fragment>
);

BottomPaginator.propTypes = {
	changePage: func.isRequired,
	addNewPage: oneOfType([func, bool]).isRequired,
	currPage: number.isRequired,
	totalPage: number.isRequired,
};

export default onlyUpdateForKeys(['currPage', 'totalPage'])(BottomPaginator);
