import PropTypes from 'prop-types';

export const PropTypesTool = PropTypes.shape({
	name: PropTypes.string.isRequired,
	stroke: PropTypes.number
});

export const PropTypesCols = PropTypes.shape({
	primary: PropTypes.string.isRequired,
	secondary: PropTypes.string.isRequired
});
