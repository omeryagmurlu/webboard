import React from 'react';
import ReactDOM from 'react-dom';
import join from 'url-join';

import { AppContainer as ReactHotLoader } from 'react-hot-loader';

const reachAppContainer = () => import(/* webpackChunkName: AppContainer */'./containers/AppContainer');

if (__BROWSER__ && __PRODUCTION__) {
	if ('serviceWorker' in navigator) {
		// eslint-disable-next-line no-undef
		navigator.serviceWorker.register(join(__webpack_public_path__, 'service-worker.js'));
	}
}

const render = (Component) => {
	ReactDOM.render(
		<ReactHotLoader>
			<Component />
		</ReactHotLoader>,
		document.getElementById('react-binding')
	);
};

const initLoad = () => reachAppContainer().then(({ default: AppContainer }) => {
	render(AppContainer);
});
if (__ELECTRON__) {
	initLoad();
} else {
	window.addEventListener('load', initLoad);
}

if (module.hot) {
	module.hot.accept('./containers/AppContainer', () => {
		initLoad();
	});
}
