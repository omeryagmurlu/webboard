import React, { Component } from 'react';
import { promisedBook } from 'app/features/Book';
import BooksView from 'app/views/BooksView';
import contextify from 'app/utils/HOCs/contextHOC';
import { setStateConnHook } from 'app/utils';

const emptyArray = [];
class BooksContainer extends Component {
	static getDerivedStateFromProps({ app }) {
		return { books: app.store.books || emptyArray };
	}

	constructor(props) {
		super(props);

		this.state = {
			selectedBookName: null,
			foreignEventUpdates: 0 // update when a foreign state change occurs to update view
		};
	}

	componentDidMount() {
		this.props.app.connection.sendToControllers('remote', 'updateBooks', this.state); // initial
		this.unwire = this.props.app.connection.wireFuncs('remote', {
			selectBook: this.selectBook,
			enterBook: this.enterBook,
			addBook: this.addBook,
			deleteBook: this.deleteBook,
			// checkDry: this.checkDry,
		});
	}

	componentWillUnmount() {
		this.props.app.connection.sendToControllers('remote', 'exitView');
		this.unwire();
	}

	setStateRespectConn = setStateConnHook(() => this, 'updateBooks')

	_getBook = (name, state) => (state || this.state).books.find(book => book.getInfo().name === name)

	selectBook = name => this.setStateRespectConn({ selectedBookName: name })

	enterBook = (name) => this.props.app.goTo(`board/${name}`)

	addBook = (stat, dryRun = false, mainCb = () => {}) => {
		const {
			name,
			dynamic,
			sources
		} = stat;
		const prom = promisedBook({
			isDynamic: dynamic,
			name,
			sources
		}, dryRun);

		return prom.then(book => {
			if (dryRun) {
				return book;
			}
			return this.props.app.setStore(prev => ({
				books: [...prev.books, book]
			}), mainCb);
		});
	};

	deleteBook = (name) => {
		this.setStateRespectConn({ selectedBookName: null });
		this.props.app.setStore(prev => { // bu belki degisir ya
			const newBooks = [...prev.books.filter(book => book.getInfo().name !== name)];
			return ({
				books: newBooks
			});
		});
	}

	render() {
		const books = this.state.books.map(book => ({
			info: book.getInfo(),
			preview: book.getPreviewImage()
		}));
		return (
			<BooksView
				books={books}
				selectedBook={books.find(book => book.info.name === this.state.selectedBookName)}

				deleteBook={this.deleteBook}
				addBook={this.addBook}
				enterBook={this.enterBook}
				selectBook={this.selectBook}

				foreignEventUpdates={this.state.foreignEventUpdates}
			/>
		);
	}
}

export default contextify(BooksContainer);
