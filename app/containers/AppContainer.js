import React, { Component } from 'react';
import _pick from 'lodash/pick';
import _get from 'lodash/get';
import _set from 'lodash/fp/set';
import _isFunction from 'lodash/isFunction';

import AppView from 'app/views/AppView';
import { Router } from 'app/features/routing';
import Disk from 'app/features/Disk';
import routes from 'app/data/routes';
import { defaults as storeDefaults, setters as storeSetters, revivers as storeRevivers } from 'app/data/store';

import { delay, exitFullScreen, enterFullScreen, guid, isMobile } from 'app/utils';
import { viewChangeTimeout } from 'app/styles/variables';

let Connection;
if (__ELECTRON__) {
	Connection = require('app/features/Connection').default; // eslint-disable-line global-require
}

const gestureFakeQueue = [];
let latInvalidate;
document.documentElement.addEventListener('click', () => {
	let tour = 0;
	const myInvalidate = guid();
	latInvalidate = myInvalidate;
	const intId = setInterval(() => {
		// console.log('runnin');
		if (latInvalidate !== myInvalidate || tour > 49) {
			clearInterval(intId);
			return;
		}
		if (gestureFakeQueue.length === 0) {
			tour++;
			return;
		}
		while (gestureFakeQueue.length > 0) {
			gestureFakeQueue.shift()();
		}
		clearInterval(intId);
	}, isMobile ? 250 : 60);
});
export function userGestureSimulation(fn) {
	gestureFakeQueue.push(fn);
}

export const AppContext = React.createContext();

const mainFeatures = __ELECTRON__ ? ['add', 'connections', 'settings'] : ['add', 'settings'];

class AppContainer extends Component {
	constructor(props) {
		super(props);

		this.state = {
			shown: true,
			isConnected: false,

			store: storeDefaults,
		};

		this.unsubscribers = [];

		this.disk = Disk({
			setStore: this.setStore,
			getStore: () => this.state.store,
			setters: storeSetters,
			revivers: storeRevivers,
		});

		this.router = Router(routes, {
			update: this._viewUpdater,
			fullscreen: this.fullscreen,
		});

		this.navigation = this.router.navigation;

		this.connection = {	wireFuncs: () => () => {}, sendToControllers() {}, setup() {} };
		if (__ELECTRON__) {
			this.connection = new Connection();
		}
		this.connHand = () => {
			const newOne = this.connection.isConnectedToRemote();
			if (newOne !== this.state.isConnected) {
				this.setState({ isConnected: newOne });
			}
		};

		this.settings = {
			get: (path) => _get(this.state.store.settings, path),
			set: (path, val) => this.setStore(prev => ({
				settings: _set(path, val, prev.settings)
			}))
		};
	}

	componentDidMount() {
		const thener = () => requestAnimationFrame(() => setTimeout(() => {
			document.documentElement.removeAttribute('data-preload');
		}, 0));
		if (document.readyState === 'complete') {
			thener();
		} else {
			window.addEventListener('load', thener);
		}

		this.disk.loadAll().then(() => {
			this.router.listen();
			if (__ELECTRON__) {
				this.connection.setup(this.settings);
				this.connection.on('disconnect', this.connHand);
				this.connection.on('connect', this.connHand);
			}
		});
	}

	componentWillUnmount() {
		this.router.unlisten();
		this.unsubscribers.forEach(unsub => unsub());
	}

	_viewUpdater = (view, isFirst, template) => {
		if (isFirst) {
			document.documentElement.setAttribute('data-route', template);
			return this.setState({ view });
		}

		this.setState({
			shown: false,
		});
		const tm1 = delay(viewChangeTimeout);
		tm1.then(() => {
			document.documentElement.setAttribute('data-route', template);
			this.setState({
				shown: true,
				view
			});
		});
		this.unsubscribers.push(tm1.cancel);
	}

	setStore = (obj, cb = () => {}) => {
		this.setState(prevState => ({ store: {
			...prevState.store,
			..._isFunction(obj) ? obj(prevState.store) : obj // buraya JSON gereksiz, surekli cekmicekdisk
		} }), () => {
			this.disk.save(); // bu ayri olcak cunku resim cekmeyi istio FIXME images
			cb();
		});
	}

	forceSave = () => this.disk.save();

	goBack = () => this.navigation.goBack()
	goTo = (path) => this.navigation.push(path)

	fullscreen = (enabled) => {
		if (this.settings.get('fullscreen') && isMobile) {
			userGestureSimulation(() => {
				if (enabled) return enterFullScreen();
				exitFullScreen();
			});
		}
	}

	render() { // simdilik sadece board var kickstart icin
		const theme = this.settings.get('theme');
		document.documentElement.setAttribute('data-theme', theme);
		return (
			<AppContext.Provider
				value={{
					store: this.state.store,
					isConnected: this.state.isConnected,
					mainFeatures,
					theme,
					..._pick(this, [
						'goBack',
						'goTo',
						'setStore',
						'forceSave',
						'connection',
						'settings',
					]),
				}}
			>
				<AppView
					view={this.state.view || <div />}
					shown={this.state.shown}
				/>
			</AppContext.Provider>
		);
	}
}

export default AppContainer;
