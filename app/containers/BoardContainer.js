/* eslint react/no-unused-state: 0 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import _pick from 'lodash/pick';
import BoardView from 'app/views/BoardView';
import { noop, setStateConnHook } from 'app/utils';
import { toolsArray, PENCIL, toolsObject } from 'app/data/tools';
import { BLACK, WHITE } from 'app/data/colors';
import contextify from 'app/utils/HOCs/contextHOC';

class BoardContainer extends Component {
	constructor(props) {
		super(props);

		this.state = {
			tool: PENCIL.name,
			color: {
				primary: BLACK,
				secondary: WHITE
			},
			tools: toolsArray.reduce((acc, curr) => {
				acc[curr.name] = _pick(curr, ['name', 'stroke']);
				return acc;
			}, {}),
			enabledTools: toolsArray.map(v => v.name),

			// savedCanvas: {},
			// previewImageFn: undefined,

			canvDataReq: undefined,
		};
	}

	static getDerivedStateFromProps({ app, book }, state) {
		if (state.book) return {};
		return { book: app.store.books.find(b => b.getInfo().name === book) };
	}

	componentDidMount() {
		this.props.app.connection.sendToControllers('remote', 'updateBoard', this.state); // initial
		this.unwire = this.props.app.connection.wireFuncs('remote', {
			changeColor: this.changeColor,
			changeTool: this.changeTool,
			changeStroke: this.changeStroke,
			changePage: this.changePage,
			exitBoard: this.exitBoard,
			insertPage: this.insertPage,
		});
	}

	componentWillUnmount() {
		this.props.app.connection.sendToControllers('remote', 'exitView');
		this.unwire();
	}

	_saveBookPage = (fn = noop) => {
		let data = {};
		if (this.state.canvDataReq) {
			data = this.state.canvDataReq();
		}
		this.state.book.getCurrentPage().updatePage({
			pageImgFn: data.preview,
			foreign: data.save,
		});
		fn();
	};

	setStateRespectConn = setStateConnHook(() => this, 'updateBoard')

	// updateSavedCanvas = ({ save, preview }) => this.setStateRespectConn({
	// 	savedCanvas: save,
	// 	previewImageFn: preview
	// })
	setCanvDataReq = fn => this.setStateRespectConn({ canvDataReq: fn })
	changePage = newPageNum =>	{
		this._saveBookPage(() => {
			const ok = this.state.book.changePage(newPageNum);
			if (ok) this.setStateRespectConn(undefined, true);
		});
	}
	insertPage = (where) => this.state.book.getInfo().dynamic && this._saveBookPage(() => {
		this.state.book.insertBlankPage(where || this.state.book.getInfo().currentPage + 1);
		this.setStateRespectConn(undefined, true);
	})

	changeTool = (newToolName => this.setStateRespectConn({ tool: newToolName }))
	changeStroke = (newStroke => this.setStateRespectConn(prevState => ({
		tools: update(prevState.tools, { [prevState.tool]: { stroke: { $set: newStroke } } })
	})))
	changeColor = ((color, type) => this.setStateRespectConn(prevState => ({
		color: update(prevState.color, { [type]: { $set: color } })
	})))
	exitBoard = () => this._saveBookPage(() => {
		this.props.app.forceSave();
		this.props.app.goTo('/');
	})

	render() {
		const shouldI = this.props.app.settings.get('clutter')
			|| !this.props.app.isConnected; // guarded merak etme
		const page = this.state.book && this.state.book.getCurrentPage().getPageData();
		return (
			<BoardView
				tool={this.state.tool}
				tools={this.state.enabledTools.map(name => toolsObject[name])}
				selectStroke={this.state.tools[this.state.tool].stroke}
				colors={this.state.color}
				shouldShowMenu={shouldI}

				{...(this.state.book ? {
					canvasRestore: page.foreign,
					canvasImg: page.bgImg,
					currPage: this.state.book.getInfo().currentPage,
					totalPage: this.state.book.getInfo().totalPage,
					changePage: this.changePage,
					addNewPage: this.insertPage
				} : {})}

				setCanvDataReq={this.setCanvDataReq}

				changeTool={this.changeTool}
				changeStroke={this.changeStroke}
				changeColor={this.changeColor}
				exitBoard={this.exitBoard}
			/>
		);
	}
}

BoardContainer.defaultProps = {
	book: null
};

BoardContainer.propTypes = {
	book: PropTypes.string
};

export default contextify(BoardContainer);
