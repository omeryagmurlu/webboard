import { ipcRenderer as ipc } from 'electron';
import { EventEmitter } from 'events';
import ip from 'ip';

class Device {
	constructor({ type, id, info }) {
		this.id = id;
		this.type = type;
		this.info = info;
	}
}

class Connection extends EventEmitter {
	constructor() {
		super();

		this.handlers = {
			remote: [],
			student: [],
		};
		this.sockets = [];
		this.isListening = false;
		this.ip = this.address = ip.address();
	}

	setup(settings) {
		this._genInitPayload = () => settings.get('theme');
		this._genCommonPayload = () => settings.get('theme');
		this.port = settings.get('connection.port'); // bu guncellenmiyor, restart atilmali, intended
		this.address = `${this.ip}:${this.port}`;
	}

	listen() {
		if (this.isListening) {
			return;
		}
		this.isListening = true;
		ipc.send('connection|init', this.port);

		return new Promise((resolve) => {
			ipc.once(`Re:connection|init:port(${this.port})`, () => {
				resolve();
			});
		});
	}

	// type: remote or student
	allowConnection(type) { // haci bu sikim gibi ama zaten iyi olmasi gerekmio
		return new Promise((resolve) => ipc.once('connection|socketConnected', (ev1, socketId, socketInfo) => {
			const device = new Device({
				id: socketId,
				type,
				info: socketInfo,
			});
			this.sockets.push(device);
			ipc.on(`connection|packet:socket(${socketId})`, (ev2, packet) => {
				this.emit('packet', socketId, packet);
				this._handlePacket(socketId, packet);
			});

			ipc.once(`connection|disconnect:socket(${socketId})`, () => {
				this.disconnect(socketId);
			});

			this.emit('connect', socketId);
			this._send(socketId, 'device-accepted', this._genInitPayload());
			resolve(socketId);
		}));
	}

	_handlePacket(id, [message, ...arg]) {
		const device = this.sockets.find(s => s.id === id);
		const handler = this.handlers[device.type][message];
		if (handler) {
			handler(...arg); // daha sonra birinci sock type olcak
		}
	}

	getConnectionCount = () => this.sockets.length
	getDevices = () => this.sockets.map(v => v)
	isConnectedToRemote = () => this.sockets.filter(v => v.type === 'remote').length > 0

	disconnect = (socketId, cb = () => {}) => {
		const ix = this.sockets.findIndex(dev => dev.id === socketId);
		if (ix !== -1) {
			this._send(socketId, 'device-dropped');
			this.sockets.splice(ix, 1);
			ipc.removeAllListeners(`connection|packet:socket(${socketId})`);
			ipc.removeAllListeners(`connection|disconnect:socket(${socketId})`);
			this.emit('disconnect', socketId);
		}
		cb();
	}

	wire = (type, message, fn) => {
		this.handlers[type][message] = fn;
	}

	unwire = (...x) => this.wire(...x, undefined);

	_send = (sockId, ...x) => {
		if (this.sockets.findIndex(dev => dev.id === sockId) !== -1) {
			ipc.send('connection|send', sockId, ...JSON.parse(JSON.stringify(x)));
		}
	}

	sendToControllers = (type, message, ...arg) => {
		this.sockets
			.filter(dev => dev.type === type)
			.forEach(dev => {
				this._send(dev.id, message, ...arg);
				this._send(dev.id, 'common-payload', this._genCommonPayload());
			});
	}

	wireFuncs = (type, obj) => {
		const unwirement = [];
		Object.keys(obj).forEach(key => {
			this.wire(type, key, obj[key]);
			unwirement.push(() => this.unwire(type, key));
		});

		this.sendToControllers(type, 'wiring', Object.keys(obj));
		return () => {
			this.sendToControllers(type, 'wiring', false);
			unwirement.forEach(uw => uw());
		};
	};
}

export default Connection;
