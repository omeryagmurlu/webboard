import _isUndefined from 'lodash/isUndefined';
import _flatten from 'lodash/flatten';
import _memoize from 'lodash/memoize';
import { noop } from 'app/utils';

const pdfAction = (pdf, dry) => import( // holy COW
	/* webpackChunkName: "PDFactions" */
	'./actions/pdf'
).then(({ readPDF, mapEveryPage, convertToImg }) => {
	const pdfProm = readPDF(pdf);
	return dry
		? pdfProm
		: pdfProm.then(mapEveryPage).then(pages => pages.map((page) =>
			() => convertToImg(1)(page)
		));
}
);

const webActionElectron = (webpages, use, dry) => import(
	/* webpackChunkName: "WEBactions" */
	`./actions/web/${__MEDIUM__}`
).then(({ getImage, checkUrl }) =>
	Promise.all(webpages.map(wURL => (dry
		? checkUrl(wURL)
		: checkUrl(wURL)
			.then(url => () => getImage(url))
	)))
);

const webActionBrowser = (/* webpages, use, dry */) => import(
	/* webpackChunkName: "WEBactions" */
	`./actions/web/${__MEDIUM__}`
).then(() => { throw new Error('Web is not implemented on Browser yet'); });

const imageAction = (images, use, dry) => import(
	/* webpackChunkName: "IMAGEactions" */
	`./actions/image/${__MEDIUM__}`
).then(({ isImg }) =>
	Promise.all(images.map(imgUrl => (dry
		? isImg(imgUrl)
		: isImg(imgUrl)
			.then(() => () => Promise.resolve(imgUrl))
	)))
);

export const promisedBook = (...x) => {
	let book;
	try {
		book = new Book(...x);
	} catch (e) {
		return Promise.reject(e);
	}
	return book.ready().then(() => book);
};

const Page = ({ bgImgFn: bgImgFnRaw = noop } = {}) => {
	let memoedBgImg = _memoize(bgImgFnRaw);
	let memoedPageImg = noop;

	const getPageData = () => ({
		foreign,
		bgImg: Promise.resolve(memoedBgImg()),
		pageImg: Promise.resolve(memoedPageImg()),
		anImage: Promise.resolve(memoedPageImg() || memoedBgImg())
	});
	const updatePage = (news) => {
		if (news.pageImgFn) memoedPageImg = _memoize(news.pageImgFn);
		if (news.bgImgFn) memoedBgImg = _memoize(news.bgImgFn);
		if (news.foreign) ({ foreign } = news);
	};

	let foreign = {};
	const toJSON = () => ({
		backgroundImage: memoedBgImg(),
		pageImage: memoedPageImg(),
		foreign
	});

	return Object.create({
		updatePage,
		getPageData,
		toJSON,
	}, {
		toJSON: {
			value: toJSON,
			writable: false
		}
	});
};
Page.revive = ({
	backgroundImage,
	pageImage,
	foreign
}) => {
	const page = Page();
	page.updatePage({
		pageImgFn: () => pageImage,
		bgImgFn: () => backgroundImage,
		foreign
	});
	return page;
};

class Book {
	static revive(deadbook) {
		const book = new Book(undefined, undefined, true);
		Object.keys(deadbook).forEach(key => (book[key] = deadbook[key]));
		book.pages = deadbook.pages.map(p => (p ? Page.revive(p) : undefined));
		book.promise = Promise.resolve();
		return book;
	}

	constructor({
		isDynamic: isDynamicParam = true,
		name: nameParam,
		sources = [],
	} = {}, dryFlag = false, __revive) {
		if (__revive) return;

		this.isDynamic = isDynamicParam;
		this.name = nameParam;

		this.origin = sources.length === 0
			? undefined
			: sources.length === 1
				? sources[0].data
				: `${sources.length} sources`;

		this.promise = Promise.resolve();

		if (!this.name) {
			throw new Error('Book needs a name');
		}

		this.pages = Array(1);
		this.currPageNum = 1;

		if (!sources.length) throw new Error('Add at least one source');
		this._queueAsync(Promise.all(sources.map(({ type, data }) => {
			switch (type) {
				case 'pdf':
					return pdfAction(data, dryFlag);
				case 'web':
					return __ELECTRON__
						? webActionElectron([data], dryFlag)
						: webActionBrowser([data], dryFlag);
				case 'image':
					return imageAction([data], dryFlag);
				case 'blank':
					return Array(parseInt(data, 10)).fill(1).map(() => () => undefined);
				default:
					throw new Error('No such source type');
			}
		})).then(_flatten)
			.then(list => !dryFlag && list.forEach(fn => this._useImg(fn)))
		);
	}

	getPage(num) {
		if (this.pages[num]) return this.pages[num];
		return this.insertBlankPage(num);
	}

	getCurrentPage() {
		return this.getPage(this.currPageNum);
	}

	insertBlankPage(num) {
		if (this.isDynamic) {
			this.pages.splice(num, 0, Page());
			return this.pages[num];
		}

		throw new Error('Book is not dynamic!');
	}

	changePage(newNum) {
		const preVal = this.currPageNum;

		let upBo = this.pages.length - 1;
		const lowBo = 1;
		if (this.isDynamic) {
			upBo = this.pages.length;
		}

		if (newNum > upBo) {
			this.currPageNum = upBo;
		} else if (newNum < lowBo) {
			this.currPageNum = lowBo;
		} else {
			this.currPageNum = newNum;
		}

		return preVal !== this.currPageNum;
	}

	setMetadata({
		dynamic: newDynamic,
		name: newName
	}) {
		if (!_isUndefined(newName)) this.name = newName;
		if (!_isUndefined(newDynamic)) this.isDynamic = newDynamic;
	}

	getPreviewImage() {
		if (this.pages.length === 1) return Promise.resolve('bunu gorunsan sictin');
		const prom = this.pages[this.currPageNum].getPageData().anImage;
		return prom;
	}

	getInfo() {
		return ({
			currentPage: this.currPageNum,
			totalPage: this.pages.length - 1,
			dynamic: this.isDynamic,
			name: this.name,
			origin: this.origin
		});
	}

	teardown() { // eslint-disable-line
		noop();
	}

	_useImg(bgImgFn) {
		this.pages.push(Page({ bgImgFn }));
	}

	ready() {
		return this.promise;
	}

	_queueAsync(newPr) {
		this.promise = newPr;
	}

	toJSON() {
		return { ...this, info: this.getInfo() };
	}
}

if (__ELECTRON__) {
	Book.sources = [
		'pdf',
		'image',
		'blank',
		'web',
	];
} else {
	Book.sources = [
		'pdf',
		'image',
		'blank',
	];
}

Book.properties = [
	'dynamic'
];

export default Book;
