import React from 'react';
import createHistory from 'history/createHashHistory';
import Route from 'route-parser';
import _pick from 'lodash/pick';
import _mapValues from 'lodash/mapValues';
import { noop } from 'app/utils';

export const Navigation = (onUpdate) => {
	const listen = (listener = onUpdate) => {
		unlisten = history.listen((location, action) => {
			listener(location, action);
		});
	};
	const getCurrentLocation = () => history.location;

	const history = createHistory();
	let unlisten = noop;

	return ({
		..._pick(history, [
			'goBack',
			'push',
			'goForward',
		]),
		listen,
		unlisten,
		getCurrentLocation,
	});
};

// update: component is react element, isFirst
export const Router = (raw, {
	update,
	fullscreen: fuller,
	index = '/'
}) => {
	const getComponent = (path) => {
		const { matcher, component, ...others } = routes.find(r => r.matcher.match(path)) || {};
		if (!matcher) {
			throw new Error(`No such route ${path}`);
		}
		return [component, matcher.match(path), others]; // 2nd params;
	};

	const adaptToLocation = (isFirst) => ({ pathname }) => {
		try {
			const [component, params, routeOpts] = getComponent(pathname);
			component().then(({ default: Component }) => {
				fuller(routeOpts.fullscreen); // dont worry, fullscreen is fool-proof

				update(<Component {...params} />, isFirst, routeOpts.template);
			});
		} catch (e) {
			console.warn(`Route doesn't exist ${pathname}, reverting to default route ${index}`);
			return nav.push(index);
		}
	};

	const normRaw = _mapValues(raw, (val) => {
		if (typeof val === 'function') {
			return { comp: val };
		}
		return val;
	});

	const routes = Object.keys(normRaw).map(str => ({
		template: str,
		component: normRaw[str].comp,
		fullscreen: normRaw[str].fullscreen || false,
		matcher: new Route(str),
	}));

	const nav = Navigation(adaptToLocation(false));

	// REVIEW: isFirst browserda olmasin, anim ciks
	adaptToLocation(__ELECTRON__)(nav.getCurrentLocation());

	return {
		navigation: nav,
		unlisten: nav.unlisten,
		listen: nav.listen,
	};
};
