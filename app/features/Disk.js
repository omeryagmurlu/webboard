import _isArray from 'lodash/isArray';
import _isNull from 'lodash/isNull';
import localforage from 'localforage';

const KEYNAME = 'key-names';

const setItem = (key, val) => localforage.setItem(key, val);
const getItem = (key) => localforage.getItem(key); // .then(JSON.parse); // yoksa null

const Disk = ({
	getStore,
	setStore,
	revivers,
	setters
}) => {
	const save = () => {
		const store = getStore();
		setItem(KEYNAME, Object.keys(store));
		Object.keys(store).forEach(key => {
			(setters[key] || ((v, cb) => cb(v)))(store[key], (trSet) => setItem(key, trSet));
		});
		return Promise.resolve();
	};

	const load = (key) =>
		getItem(key).then(walle => setStore({ [key]: (revivers[key] || (v => v))(walle) }));

	const loadAll = () => getItem(KEYNAME).then(keys => {
		if (_isArray(keys)) {
			return Promise.all(keys.map(key => getItem(key).then(walle =>
				(revivers[key] || (v => v))(walle)
			))).then(vals => {
				const store = vals.reduce((acc, val, i) => {
					acc[keys[i]] = val;
					return acc;
				}, {});

				setStore(store);
			});
		}
	});

	const keyExist = (key) => getItem(key).then(val => !_isNull(val));
	return {
		save,
		load,
		loadAll,
		keyExist,
	};
};

export default Disk;
