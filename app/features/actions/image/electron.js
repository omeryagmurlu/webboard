import fs from 'mz/fs';
import isImage from 'is-image';
import { fetchControl } from './common';

export const isImg = async (url) => {
	if (url.match('://')) {
		return fetchControl(url);
	}

	if (!isImage(url)) {
		throw new Error(`No Image Extension: ${url}`);
	}

	if (!await fs.exists(url)) {
		throw new Error(`file doesnt exist: ${url}`);
	}

	return url;
};

export const amq = 5;
