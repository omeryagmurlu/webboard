export const fetchControl = (url) => fetch(url, {
	method: 'GET',
}).then(({ ok, headers, status }) => {
	if (!ok) throw new Error(`Not OK ${status} ${url}`);
	if (!headers.get('Content-Type').match('image')) throw new Error(`${url} is not an image`);
	return url;
});

export const amq = 5;
