import http from 'http';
import urlLib from 'url';
import { ipcRenderer as ipc } from 'electron';

export const checkUrl = (url) => {
	const options = {
		method: 'HEAD',
		host: urlLib.parse(url).host,
		port: 80,
		path: urlLib.parse(url).pathname
	};

	return new Promise((resolve, reject) => {
		const req = http.request(options, r => {
			if (!String(r.statusCode).match(/^4\d\d$/)) {
				return resolve(url);
			}
			reject(new Error(`Can't resolve ${url}`));
		});
		req.on('error', reject);
		req.end();
	});
};

export const getImage = (url) => new Promise((resolve) => {
	ipc.send('webpageToImage|getImage', url, window.innerWidth, window.innerHeight);

	ipc.once(`Re:webAction|getImage:arg(${url})`, (event, image) => {
		resolve(image);
	});
});
