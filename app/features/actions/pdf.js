import pdfjs from 'pdfjs-dist';
import pdfWorkerPath from 'pdfjs-dist/build/pdf.worker.min.js';

pdfjs.GlobalWorkerOptions.workerSrc = pdfWorkerPath;

export const readPDF = (url) => pdfjs.getDocument(url);

export const mapEveryPage = (pdf) => Promise.all(Array(pdf.pdfInfo.numPages).fill(1)
	.map((_, i) => pdf.getPage(i + 1)));

export const convertToImg = (scale = 1) => (page) => {
	const canvas = document.createElement('canvas');
	const context = canvas.getContext('2d');
	const viewport = page.getViewport(scale);
	canvas.height = viewport.height;
	canvas.width = viewport.width;

	return page.render({ canvasContext: context, viewport }).then(() => canvas.toDataURL());
};
