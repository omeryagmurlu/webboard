import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withState, compose } from 'recompose';
import chroma from 'chroma-js';
import IconButton from '@material-ui/core/IconButton';

import Menu from '@material-ui/icons/Menu';
import Close from '@material-ui/icons/Close';

import Divton from 'app/components/Divton';
import Canvas from 'app/components/Canvas';
import StrokeMeter from 'app/components/StrokeMeter';
import ColorBrush from 'app/components/ColorBrush';
import withTheme from 'app/utils/HOCs/withThemeHOC';
import { simpleLazy } from 'app/utils/lazy';
import { toolIconProcess } from 'app/utils';
import threeSwipe from 'app/utils/HOCs/threeSwipeHOC';
import { PropTypesCols } from 'app/prop-schema';
import colorList from 'app/data/colors';
import { toolsObject } from 'app/data/tools';

import commonStyle from 'app/styles/common.scss';
import style from './style.scss';

let css; // REVIEW: only 1 board is open at a moment, no problem

const BottomPaginator = simpleLazy(() => import(/* webpackChunkName: "BottomPaginator" */ 'app/components/BottomPaginator'));

const LeftSwipey = threeSwipe({
	lean: 'left'
}, ['selected'])(Divton);

// eslint-disable-next-line react/prop-types
const Closer = ({ onClick }) => (
	<IconButton
		onClick={onClick}
		aria-label="save and exit board"
	>
		<Close />
	</IconButton>
);

// eslint-disable-next-line react/prop-types
const Hamburger = ({ menuState, setMenuState }) => (
	<IconButton
		className={css(['hamb', !menuState && 'shrinked'])}
		onClick={() => setMenuState(!menuState)}
		aria-label="toggle menu"
	>
		<Menu />
	</IconButton>
);

// eslint-disable-next-line react/prop-types
const LeftBar = ({ tools, curCols, currToolName, changeTool }) => (
	<div className={css(['toolset', 'left', 'aletmenu'])}>
		{tools.map(tool => {
			const { name: toolName } = tool;
			const { fills, svgProps, Icon, iconStyle } = toolIconProcess(tool, curCols);
			return (
				<div key={toolName} className={css('tool-cont')} >
					<LeftSwipey
						key={toolName}
						className={css('tool')}
						selected={currToolName === toolName}
						onClick={() => changeTool(toolName)}
						aria-label={toolName}
						style={{
							background: Icon && 'transparent'
						}}
					>
						{Icon &&
							<Icon
								{...fills}
								className={css('icon-svg')}
								style={{
									filter: `saturate(1.50) sepia(0.3) brightness(0.9) drop-shadow(0 0 15px ${fills.fill1})`,
									float: 'right',
									...iconStyle,
								}}
								{...svgProps}
							/>
						}
					</LeftSwipey>
				</div>
			);
		})}
	</div>
);
// eslint-disable-next-line react/prop-types
const RightBar = ({ currToolName, curCols, changeColor, selectStroke, changeStroke }) => (
	<Fragment>
		<div className={css(['toolset', 'right', 'renkmenu'])}>
			{colorList.map(color => (
				<div className={css('color-brush-container')} key={color} >
					<ColorBrush
						key={color}
						color={color}
						currSecColor={toolsObject[currToolName].secondaryColor && curCols.secondary}
						onPrimary={() => changeColor(color, 'primary')}
						onSecondary={() => changeColor(color, 'secondary')}

						selected={chroma(curCols.primary).css() === chroma(color).css()}
						hidden={!toolsObject[currToolName].primaryColor}
					/>
				</div>
			))}
		</div>
		<div className={css('stroke-cont')}>
			<StrokeMeter
				onClick={num => changeStroke(num)}
				currStroke={selectStroke}

				hidden={!selectStroke}
				hideRight
			/>
		</div>
	</Fragment>
);
// eslint-disable-next-line react/prop-types
const BottomBar = ({ bottomFeatures, menuState }) => (
	<div className={css(['bottom-bar', !menuState && 'shrinked', (bottomFeatures[0].length || bottomFeatures[1].length) ? '' : 'hidden'])}>
		{bottomFeatures[0].map((v, i) => (
			<div
				key={v.type.displayName}
				className={css(['bot-subdiv', i !== bottomFeatures[0].length - 1 ? '' : 'first-last'])}
			>
				{v}
			</div>
		))}
		{bottomFeatures[1].map((v, i) => <div key={v} className={css(['bot-subdiv', i ? '' : 'second-first'])}>{v}</div>)}
	</div>
);

const BoardView = ({
	tool: currToolName,
	tools,
	colors: curCols,
	selectStroke,
	currPage,
	totalPage,

	changeTool,
	changeStroke,
	changeColor,
	changePage,
	addNewPage,
	exitBoard,

	css: innerCss,
	menuState,
	setMenuState,
	shouldShowMenu,

	...propsToCanvas
}) => {
	css = innerCss; // REVIEW

	const isPaginator = () => currPage && (totalPage > 1 || addNewPage);

	const bottomFeatures = shouldShowMenu &&
		!menuState ? [[<Hamburger {...{ menuState, setMenuState }} />], []] : [[
			<Hamburger {...{ menuState, setMenuState }} />,
			isPaginator() && <BottomPaginator {...{ currPage, totalPage, changePage, addNewPage }} />,
		], [
			<Closer onClick={exitBoard} />
		]].map(arr => arr.filter(v => v));

	return (
		<div className={css('container')}>
			<Canvas
				toolName={currToolName}
				toolStroke={selectStroke}
				colors={curCols}
				changeColor={changeColor}
				{...propsToCanvas}
			/>

			<LeftBar {...{ tools, curCols, currToolName, changeTool }} />
			<RightBar {...{ curCols, currToolName, changeColor, selectStroke, changeStroke }} />
			{shouldShowMenu && <BottomBar
				{...{ bottomFeatures, menuState }}
			/>}
		</div>
	);
};

BoardView.defaultProps = {
	changePage() {},
	selectStroke: null,
	currPage: null,
	totalPage: null,
	addNewPage: false
};

BoardView.propTypes = {
	tool: PropTypes.string.isRequired,
	tools: PropTypes.array.isRequired,
	selectStroke: PropTypes.number,
	colors: PropTypesCols.isRequired,
	shouldShowMenu: PropTypes.bool.isRequired,

	totalPage: PropTypes.number,
	currPage: PropTypes.number,
	changePage: PropTypes.func,
	addNewPage: PropTypes.oneOfType([
		PropTypes.func,
		PropTypes.bool
	]),
	exitBoard: PropTypes.func.isRequired,

	changeTool: PropTypes.func.isRequired,
	changeStroke: PropTypes.func.isRequired,
	changeColor: PropTypes.func.isRequired,

	css: PropTypes.any.isRequired,
	menuState: PropTypes.bool.isRequired,
	setMenuState: PropTypes.func.isRequired,
};

export default compose(
	withTheme({ ...commonStyle, ...style }),
	withState('menuState', 'setMenuState', false)
)(BoardView);
