import React, { Fragment } from 'react';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import Snackbar from '@material-ui/core/Snackbar';
import Typography from '@material-ui/core/Typography';

import EmptyStateText from 'app/components/EmptyStateText';

import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';

import { stylist, INTERPUNKT_TEXT } from 'app/utils';
import commonStyle from 'app/styles/common.scss';
import style from './style.scss';
import { PanelOutside } from '../';

const css = stylist({ ...commonStyle, ...style });

const MESSAGES = [
	null,
	'Connect your device now'
];

const deviceLister = (typeName, devices, deleteOne) => (
	<List
		subheader={<ListSubheader disableSticky component="div">Connected {typeName}</ListSubheader>}
	>
		{devices.map((device) => (
			<ListItem
				key={device.id}
			>
				<ListItemText
					primary={device.info.description}
					secondary={`${device.type}${INTERPUNKT_TEXT}${device.id}`}
				/>
				<ListItemSecondaryAction>
					<IconButton aria-label="Delete" onClick={() => deleteOne(device.id)}>
						<Delete />
					</IconButton>
				</ListItemSecondaryAction>
			</ListItem>
		))}
	</List>
);

const Connections = ({
	addNew,
	deleteOne,
	canAdd,
	devices,
	message,
	maxDevCount,
	address,
}) => {
	const remotes = devices.filter(d => d.type === 'remote');
	const students = devices.filter(d => d.type === 'student');
	return (
		<Fragment>
			<PanelOutside
				title="Connections"
				desc="Add or revoke device connections"
			>
				<div className={css('butts')}>
					<Typography className={css('typer')} variant="caption">
						network address: <strong>{address}</strong>
					</Typography>
					<Typography className={css('typer')} variant="caption">{`max connections: ${devices.length ? `${devices.length}/` : ''}${maxDevCount}`}</Typography>
					<Button className={css('butt-on')} disabled variant="raised" color="primary" onClick={() => addNew('student')}>Add Student</Button>
					<Button className={css('butt-on')} disabled={!canAdd} variant="raised" color="primary" onClick={() => addNew('remote')}>
						Add Remote
						<Add className={css('text-button-icon-right')} />
					</Button>
				</div>
				{remotes.length > 0 && deviceLister('Remotes', remotes, deleteOne)}
				{students.length > 0 && deviceLister('Students', students, deleteOne)}
				<Snackbar
					anchorOrigin={{
						vertical: 'bottom',
						horizontal: 'left',
					}}
					open={Boolean(message)}
					autoHideDuration={6000}
					message={MESSAGES[message]}
				/>
			</PanelOutside>
			<EmptyStateText
				primary="Connect devices!"
				secondary="Devices you connect will show up here"
				isEmpty={remotes.length + students.length <= 0}
			/>
		</Fragment>
	);
};

export default Connections;
