import React from 'react';
import PropTypes from 'prop-types';
import { lifecycle } from 'recompose';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Radio from '@material-ui/core/Radio';
import Checkbox from '@material-ui/core/Checkbox';

import Save from '@material-ui/icons/Save';

import ButtonedTextInput from 'app/components/ButtonedTextInput';

import { stylist } from 'app/utils';
import commonStyle from 'app/styles/common.scss';
import style from './style.scss';
import { PanelOutside, PanelSection } from '../';

const css = stylist({ ...commonStyle, ...style });

// eslint-disable-next-line react/prop-types
const makeForm = (name, helper, remainder) => (
	<FormControl className={css('form-cont')} fullWidth={false} >
		<FormLabel component="legend" className={css('form-label')} >{name}</FormLabel>
		{remainder}
		{helper && (
			<FormHelperText className={css('helper')}>{helper}</FormHelperText>
		)}
	</FormControl>
);
const MyRadio = ({ current, onChange, name, options, helper }) => makeForm(name, helper, (
	<RadioGroup
		aria-label={name}
		name={name}
		value={current}
		onChange={e => onChange(e.target.value)}
	>
		{options.map(val => (
			<FormControlLabel className={css('radio')} key={val} value={val} control={<Radio />} label={val} />
		))}
	</RadioGroup>
));
const MyCheckbox = ({ current, onChange, name, id, helper }) => makeForm(name, helper, (
	<FormControlLabel
		control={
			<Checkbox
				checked={current}
				onChange={e => onChange(e.target.checked)}
				value={id || name}
			/>
		}
		label={id || name}
	/>
));
const MyInput = ({ current, onChange, name, helper, ...pass }) => makeForm(name, helper, (
	<ButtonedTextInput
		className={css('button-input')}
		onClick={onChange}
		value={current}
		icon={Save}
		{...pass}
	/>
));
// <FormControl fullWidth>
// <InputLabel htmlFor={id || name}>{name}</InputLabel>
// <Select
// value={current}
// onChange={e => onChange(e.target.value)}
// inputProps={{
// 	name: id || name,
// 	id: id || name,
// }}
// >
// {options.map(val => (
// 	<MenuItem key={val} value={val}>{val}</MenuItem>
// ))}
// </Select>
// </FormControl>

const Settings = ({
	theme,
	themes,
	setTheme,

	fullscreen,
	setFullscreen,
	connection,
	connPort,
	setConnPort,
	connMax,
	setConnMax,

	clutter,
	setClutter,
}) => (
	<PanelOutside
		title="Settings"
		desc="Tweak application settings"
		innerClassName={css('cont')}
	>
		<PanelSection
			title="General settings"
			innerClassName={css('section')}
		>
			<MyRadio
				current={theme}
				options={themes}
				onChange={setTheme}
				name="Theme"
			/>
			<MyCheckbox
				current={fullscreen}
				onChange={setFullscreen}
				name="Allow Fullscreen"
				id="fullscreen"
				helper="On certain situations, allow app to use fullscreen"
			/>
		</PanelSection>
		{connection && (
			<PanelSection
				title="Connection settings"
				desc="These settings take effect after restarting"
				innerClassName={css('section')}
			>
				<MyInput
					className={css(['num-inp', 'button-input'])}
					current={connPort}
					onChange={x => setConnPort(parseInt(x, 10))}
					name="Port"
					type="number"
					validator={x => parseInt(x, 10) > 1024 && parseInt(x, 10) <= 65535}
					errText="Port must be between 1024 and 65535"
					helper="Port serving remotes on the network"
				/>
				<MyInput
					className={css(['num-inp', 'button-input'])}
					current={connMax}
					onChange={x => setConnMax(parseInt(x, 10))}
					name="Max Connections"
					type="number"
					validator={x => parseInt(x, 10) >= 0}
					helper="Maximum connected devices at a moment"
				/>
				<MyCheckbox
					current={clutter}
					onChange={setClutter}
					name="Show Clutter"
					id="clutter"
					helper="Do not hide unnecessary controls when a remote is connected"
				/>
			</PanelSection>
		)}
	</PanelOutside>
);

Settings.propTypes = {
	theme: PropTypes.string.isRequired,
	themes: PropTypes.array.isRequired,
	setTheme: PropTypes.func.isRequired,
	fullscreen: PropTypes.bool.isRequired,
	setFullscreen: PropTypes.func.isRequired,

	connection: PropTypes.bool.isRequired,

	connPort: PropTypes.number.isRequired,
	setConnPort: PropTypes.func.isRequired,
	connMax: PropTypes.number.isRequired,
	setConnMax: PropTypes.func.isRequired,

	clutter: PropTypes.bool.isRequired,
	setClutter: PropTypes.func.isRequired,
};

export default lifecycle({
	componentDidMount() {
		const elems = document.querySelectorAll(`.${css('form-label')}`);
		const len = Array.from(elems)
			.reduce((acc, elem) => Math.max(acc, elem.getBoundingClientRect().width), 0);
		elems.forEach(elem => (elem.style.width = `${len}px`));
	}
})(Settings);
