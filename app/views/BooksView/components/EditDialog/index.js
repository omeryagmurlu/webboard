import React from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import targetEvent from 'app/utils/HOCs/targetEventHOC';

import LabeledCheckbox from 'app/components/LabeledCheckbox';
import Book from 'app/features/Book';

import { stylist } from 'app/utils';
import style from './style.scss';

const css = stylist({ ...style });

const TargetedTextField = targetEvent(TextField, 'value', 'onChange');
const TargetedLabeledCheckbox = targetEvent(LabeledCheckbox, 'checked', 'onChange');

const MAP = {
	dynamic: ({ dynamic, handleDynamic }) => (<TargetedLabeledCheckbox
		label="dynamic"
		value={dynamic}
		onChange={handleDynamic}
	/>),
};

const EditDialog = (props) => {
	const {
		name,
		handleNameText,
		handleSendClick,
		isButtonDisabled,
	} = props;
	return (
		<div>
			<DialogTitle>Edit Book</DialogTitle>
			<DialogContent className={css('dia-content')}>
				<div className={css('uppercont')}>
					<TargetedTextField
						fullWidth
						id="name"
						value={name}
						onChange={handleNameText}
						label="Name"
					/>
					<div className={css('features')}>
						{(Book.properties
							.map(feat => console.log(feat) || (
								<div
									key={feat}
									className={css('untertanen')}
								>
									{MAP[feat](props)}
								</div>
							)))}
					</div>
				</div>
			</DialogContent>
			<DialogActions>
				<Button onClick={handleSendClick} disabled={isButtonDisabled}>
					Save Book
				</Button>
			</DialogActions>
		</div>
	);
};

export default EditDialog;
