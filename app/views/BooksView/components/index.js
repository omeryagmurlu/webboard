import React, { Fragment } from 'react';
import Typography from '@material-ui/core/Typography';

import { stylist } from 'app/utils';

import genStyle from './style.scss';

const css = stylist({ ...genStyle });

export const PanelOutside = ({
	title,
	desc,
	footer = null,
	innerClassName = '',
	children,
}) => (
	<Fragment>
		<div className={css('container')}>
			<Typography variant="display2" paragraph={!desc}>{title}</Typography>
			{desc && <Typography variant="headline" paragraph>{desc}</Typography>}
			<div className={`${innerClassName}`}>
				{children}
			</div>
		</div>
		{footer}
	</Fragment>
);

export const PanelSection = ({
	title,
	desc,
	innerClassName = '',
	children
}) => (
	<div className={css('section')}>
		<Typography variant="title">{title}</Typography>
		{desc && <Typography variant="subheading">{desc}</Typography>}
		<div className={`${css('section-inner')} ${innerClassName}`}>
			{children}
		</div>
	</div>
);

export const amqq = 5;
