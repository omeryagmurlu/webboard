import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { compose, withHandlers } from 'recompose';
import flatten from 'lodash/flatten';
import update from 'immutability-helper';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

import AttachFile from '@material-ui/icons/AttachFile';
import Send from '@material-ui/icons/Send';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';

import EmptyStateText from 'app/components/EmptyStateText';
import LabeledCheckbox from 'app/components/LabeledCheckbox';
import ButtonedTextInput from 'app/components/ButtonedTextInput';
import FileButton from 'app/components/FileButton';
import { stylist, onMouseOver } from 'app/utils';
import { stateHOC } from 'app/utils/HOCs';
import targetEvent from 'app/utils/HOCs/targetEventHOC';

import commonStyle from 'app/styles/common.scss';
import style from './style.scss';
import { PanelOutside, PanelSection } from '../';

const css = stylist({ ...commonStyle, ...style });

const TargetedTextField = targetEvent(TextField, 'value', 'onChange');
const TargetedLabeledCheckbox = targetEvent(LabeledCheckbox, 'checked', 'onChange');

const inputNamesMap = {
	pdfW: 'PDF',
	imageW: 'Image',
	blankW: 'Blank Page',
	webW: 'Webpage'
};

const inputsMap = {
	blankW: (pushSource) => (
		<ButtonedTextInput
			value={1}
			autoReset
			key="blank"
			label="Blank Pages"
			type="number"
			side="end"
			fullWidth
			icon={Send}
			onClick={pushSource('blankW')}
			validator={x => parseInt(x, 10) >= 0}
			className={css(['gener-type', 'blank'])}
		/>
	),
	pdfW: (pushSource) => (
		<FileButton
			key="pdf"
			ext="pdf"
			onChange={pushSource('pdfW')}
			limit
			multiple
			component={Button}
			size="small"
			className={css('gener-type')}
		>
			Add PDFs
			<AttachFile className={css('text-button-icon-right')} />
		</FileButton>
	),
	imageW: (pushSource) => (
		<FileButton
			key="image"
			onChange={pushSource('imageW')}
			multiple
			component={Button}
			size="small"
			className={css('gener-type')}
		>
			Add Images
			<AttachFile className={css('text-button-icon-right')} />
		</FileButton>
	),
	webW: (pushSource) => (
		<ButtonedTextInput
			autoReset
			key="web"
			label="Website"
			side="end"
			fullWidth
			icon={Send}
			onClick={pushSource('webW')}
			className={css(['gener-type', 'web'])}
		/>
	),
};

const AddBook = ({
	addBook,
	checkDry,
	canAdd,
	sourceTypes,
	// recompose
	state,
	handleNameText,
	handleDynamic,
	pushSource,
	removeSource
}) => (
	<PanelOutside
		title="Add Book"
		desc="Use options below to add a new book."
		footer={(
			<Fragment>
				<div
					className={css('floating-action-button')}
					{...onMouseOver(() => checkDry(state))}
					style={{
						width: '50px',
						height: '50px',
					}}
				/>
				<Button
					variant="fab"
					color="primary"
					className={css('floating-action-button')}
					disabled={!canAdd}
					onClick={() => canAdd && addBook(state)}
				>
					<Add />
				</Button>
			</Fragment>
		)}
	>
		<PanelSection
			title="Properties"
			desc="Enable specific features. These options are later changeable."
			innerClassName={css('properties')}
		>
			<TargetedTextField
				id="name"
				value={state.name}
				onChange={handleNameText}
				label="Name"
				className={css('prop')}
			/>
			<TargetedLabeledCheckbox
				label="dynamic"
				value={state.dynamic}
				onChange={handleDynamic}
				className={css('prop')}
			/>
		</PanelSection>
		<PanelSection
			title="Generation Options"
			desc="Add one or more sources to generate your book from."
			innerClassName={css('generation')}
		>
			<div className={css('gener-types')}>
				{sourceTypes.map(type => inputsMap[type](pushSource))}
			</div>
			<List
				className={css('generated-entries')}
				subheader={<ListSubheader disableSticky component="div">Sources</ListSubheader>}
			>
				{flatten(state.generation).map(({ type, data, timestamp }, i) => (
					<ListItem
						key={`${type}+${data}+${timestamp}`}
					>
						<ListItemText primary={inputNamesMap[type]} secondary={data} />
						<ListItemSecondaryAction>
							<IconButton aria-label="Delete" onClick={() => removeSource(i)}>
								<Delete />
							</IconButton>
						</ListItemSecondaryAction>
					</ListItem>
				))}
				<EmptyStateText
					primary="Add sources!"
					secondary="No source added"
					isEmpty={flatten(state.generation).length <= 0}
				/>
			</List>
		</PanelSection>
	</PanelOutside>
);

AddBook.propTypes = {
	addBook: PropTypes.func.isRequired,
	checkDry: PropTypes.func.isRequired,
	canAdd: PropTypes.bool.isRequired,
	sourceTypes: PropTypes.arrayOf(PropTypes.string).isRequired,
	// recompose
	state: PropTypes.object.isRequired,
	pushSource: PropTypes.func.isRequired,
	removeSource: PropTypes.func.isRequired,
	handleNameText: PropTypes.func.isRequired,
	handleDynamic: PropTypes.func.isRequired,
};

const handler = (propName, doDry = true) => ({ setState, checkDry }) => (val) => {
	setState({ [propName]: val }, newS => {
		if (doDry) checkDry(newS);
	});
};

const makeSrc = (type, data) => {
	if (Array.isArray(data)) {
		return data.map(oneData => makeSrc(type, oneData)[0]);
	}
	return [{ type, data, timestamp: Date.now() }];
};

export default compose(
	stateHOC({
		name: '',
		dynamic: true,
		generation: [],
		// genTypes
	}, 'state', 'setState'),
	withHandlers({
		pushSource: ({ setState, state, checkDry }) => (genType) =>
			data => {
				setState({ generation: [...state.generation, ...makeSrc(genType, data)] }, (newState) => {
					checkDry(newState);
				});
			},
		removeSource: ({ setState, state, checkDry }) => (idx) => {
			setState({ generation: update(state.generation, { $splice: [[idx, 1]] }) }, (newState) => {
				checkDry(newState);
			});
		},
		handleNameText: handler('name'),
		handleDynamic: handler('dynamic'),
	}),
)(AddBook);
