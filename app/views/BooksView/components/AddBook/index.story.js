import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import AddBook from './index';

storiesOf('AddBook', module)
	.add('default', () => (
		<AddBook
			addBook={action('addBook')}
			checkDry={action('checkDry')}
			canAdd
			sourceTypes={[
				'pdfW',
				'imageW',
				'blankW',
				// 'webW',
			]}
		/>
	));
