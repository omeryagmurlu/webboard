import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _pick from 'lodash/pick';

import contextify from 'app/utils/HOCs/contextHOC';
import Book from 'app/features/Book';

import EditDialog from '../components/EditDialog';

const emptyArray = [];
class EditDialogContainer extends Component {
	static getDerivedStateFromProps({ app }) {
		return { books: app.store.books || emptyArray };
	}

	constructor(props) {
		super(props);

		this.state = {
			...this.state,
			..._pick(
				this.props.selectedBook.info,
				Book.properties.concat(['name'])
			),
		};
	}

	editBookMetadata = () => { // boyle daha mi guzel?
		const book = this.state.books
			.find(bk => bk.getInfo().name === this.props.selectedBook.info.name);
		book.setMetadata(this.state);
		this.props.selectBook(this.state.name); // yoksa bile update ol
		this.props.app.forceSave();
	}

	handle = (what) => cvp => this.setState({ [what]: cvp });

	render() {
		return (
			<EditDialog
				name={this.state.name}
				dynamic={this.state.dynamic}
				handleNameText={this.handle('name')}
				handleDynamic={this.handle('dynamic')}
				handleSendClick={this.editBookMetadata}
			/>
		);
	}
}

EditDialogContainer.propTypes = {
	selectedBook: PropTypes.object.isRequired,
	selectBook: PropTypes.func.isRequired
};

export default contextify(EditDialogContainer);
