import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withFromApp from 'app/utils/HOCs/withFromAppHOC';
import Connections from '../components/Connections';

class ConnectionsContainer extends Component {
	constructor(props) {
		super(props);
		this.props.connection.listen();

		this.state = {
			message: 0,
		};

		this.eventHandler = () => {
			// forceUpdate gorevini goruo bi nebze
			this.setState({
				message: 0,
			});
		};
	}

	componentDidMount() {
		this.props.connection.on('connect', this.eventHandler);
		this.props.connection.on('disconnect', this.eventHandler);
	}

	componentWillUnmount() {
		this.props.connection.removeListener('connect', this.eventHandler);
		this.props.connection.removeListener('disconnect', this.eventHandler);
	}

	getMaxConn = () => this.props.settings.get('connection.maxConnections')
	canIAdd = () => this.props.connection.getConnectionCount() < this.getMaxConn()

	addNew = (type) => {
		if (!this.canIAdd()) {
			return;
		}
		this.setState({
			message: 1,
		}); // arka arkaya tiklayinca birden fazla allow etme
		this.props.connection.allowConnection(type);
	}

	deleteOne = id => {
		this.props.connection.disconnect(id);
		this.forceUpdate();
	}

	render() {
		return (
			<Connections
				addNew={this.addNew}
				deleteOne={this.deleteOne}

				canAdd={this.state.message === 0 && this.canIAdd()}
				maxDevCount={this.getMaxConn()}
				devices={this.props.connection.getDevices()}
				message={this.state.message}
				address={this.props.connection.address}
			/>
		);
	}
}

ConnectionsContainer.propTypes = {
	connection: PropTypes.any.isRequired,
	settings: PropTypes.any.isRequired,
};

export default withFromApp(['connection', 'settings'])(ConnectionsContainer);
