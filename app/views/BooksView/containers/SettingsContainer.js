import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withFromApp from 'app/utils/HOCs/withFromAppHOC';
import Settings from '../components/Settings';

const THEME_NAMES = ['dark', 'giant-goldfish', 'sugar', 'thought-provoking', 'cheer-up', 'fresh-cut'];

class SettingsContainer extends Component {
	setTheme = x => this.props.settings.set('theme', x);
	setFullscreen = x => this.props.settings.set('fullscreen', x);
	setConnPort = x => this.props.settings.set('connection.port', x);
	setConnMax = x => this.props.settings.set('connection.maxConnections', x);
	setClutter = x => this.props.settings.set('clutter', x);

	render() {
		return (
			<Settings
				theme={this.props.settings.get('theme')}
				themes={THEME_NAMES}
				setTheme={this.setTheme}

				fullscreen={this.props.settings.get('fullscreen')}
				setFullscreen={this.setFullscreen}

				connection={__ELECTRON__}
				connPort={this.props.settings.get('connection.port')}
				setConnPort={this.setConnPort}
				connMax={this.props.settings.get('connection.maxConnections')}
				setConnMax={this.setConnMax}

				clutter={this.props.settings.get('clutter')}
				setClutter={this.setClutter}
			/>
		);
	}
}

SettingsContainer.propTypes = {
	settings: PropTypes.any.isRequired,
};

export default withFromApp(['settings'])(SettingsContainer);
