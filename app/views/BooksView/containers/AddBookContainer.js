import React, { Component } from 'react';
import contextify from 'app/utils/HOCs/contextHOC';
import AddBook from '../components/AddBook';

const emptyArray = [];
class AddBookContainer extends Component {
	static getDerivedStateFromProps({ app }) {
		return { books: app.store.books || emptyArray };
	}

	static mapViewStateToBookOpts({ generation, ...rest }) {
		return {
			...rest,
			sources: generation.map(({ type, data }) => ({
				data, type: AddBookContainer.mapViewTypeToBook[type]
			}))
		};
	}

	static viewSources = ['pdfW', 'imageW', 'blankW', 'webW']
	static mapViewTypeToBook = {
		pdfW: 'pdf',
		imageW: 'image',
		blankW: 'blank',
		webW: 'web'
	}

	constructor(props) {
		super(props);

		this.state = {
			canAdd: false
		};
	}

	addBook = (viewState, dry, shet) =>
		this.props.addBook(
			AddBookContainer.mapViewStateToBookOpts(viewState), dry, shet || (() => this.props.toClose())
		)

	checkDry = (arg) => this.addBook(arg, true, () => {}).then(() => {
		this.setState({ canAdd: true });
	}).catch(() => {
		this.setState({ canAdd: false });
	})

	render() {
		return (
			<AddBook
				addBook={this.addBook}
				checkDry={this.checkDry}
				canAdd={this.state.canAdd}
				sourceTypes={AddBookContainer.viewSources}
			/>
		);
	}
}

export default contextify(AddBookContainer);
