import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import _flow from 'lodash/flow';
import { withState } from 'recompose';

import Async from 'react-promise';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import ButtonBase from '@material-ui/core/ButtonBase';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Zoom from '@material-ui/core/Zoom';

import SpecialPaper from 'app/components/SpecialPaper';
import ImageFrame from 'app/components/ImageFrame';

import Delete from '@material-ui/icons/Delete';
import Edit from '@material-ui/icons/Edit';
import PlayArrow from '@material-ui/icons/PlayArrow';
import Add from '@material-ui/icons/Add';
import Settings from '@material-ui/icons/Settings';
import DeviceHub from '@material-ui/icons/DeviceHub';
import Close from '@material-ui/icons/Close';

import withTheme from 'app/utils/HOCs/withThemeHOC';
import withFromApp from 'app/utils/HOCs/withFromAppHOC';
import withDialog from 'app/utils/HOCs/withDialogHOC';
import { simpleLazy } from 'app/utils/lazy';
import { isMobile, bookInfoTexter } from 'app/utils';

import commonStyles from 'app/styles/common.scss';
import style from './style.scss';

const EditDialog = simpleLazy(() => import(/* webpackChunkName: "EditDialog" */ './containers/EditDialog'));

const featIconMap = {
	add: Add,
	settings: Settings,
	connections: DeviceHub
};

const featNameMap = {
	add: 'Add Book',
	settings: 'Settings',
	connections: 'Collaboration'
};

const featContainerMap = {
	add: () => import(/* webpackChunkName: AddBookContainer */ './containers/AddBookContainer'),
	settings: () => import(/* webpackChunkName: SettingsContainer */ './containers/SettingsContainer'),
	connections: () => {
		if (__ELECTRON__) {
			return import(/* webpackChunkName: SettingsContainer */ './containers/ConnectionsContainer');
		}
	},
};

// eslint-disable-next-line react/prop-types
const NPaper = ({ comp: Comp = Paper, css, children, nPaperInnerCName, ...props }) => (
	<Comp {...props}>
		<div className={`${css('n-paper')} ${nPaperInnerCName}`}>
			{children}
		</div>
	</Comp>
);
// eslint-disable-next-line react/prop-types
const FeatButton = ({ css, feat, onClick }) => {
	const Icon = featIconMap[feat];
	return (
		<ButtonBase className={css('feat-button')} onClick={onClick} component="button" >
			<div>
				<Icon color="inherit" className={css('feat-icon')} />
				<Typography color="inherit">{featNameMap[feat]}</Typography>
			</div>
		</ButtonBase>
	);
};

// eslint-disable-next-line react/prop-types
const ButtonsPanel = ({ props, css, isShown, selectedLeft, setSelectedLeft, mainFeatures }) => (
	<Fragment>
		<NPaper
			comp={SpecialPaper}
			css={css}
			className={css(['navs', isShown && 'shown', selectedLeft && 'grown'])}
			nPaperInnerCName={css('inner')}
		>
			{mainFeatures.map(feat => (
				<FeatButton
					css={css}
					key={feat}
					feat={feat}
					onClick={() => setSelectedLeft(feat)}
					isActive={selectedLeft === feat}
				/>
			))}
		</NPaper>
		<NPaper
			comp="div"
			css={css}
			className={css(['feat-container', isShown && 'shown', selectedLeft && 'grown'])}
			nPaperInnerCName={css('inner')}
		>
			{selectedLeft && (
				<Fragment>
					<Async
						promise={featContainerMap[selectedLeft]()}
						then={({ default: Comp }) => <Comp {...props} toClose={() => setSelectedLeft(null)} />}
					/>
					<IconButton aria-label="close" className={css('close')} onClick={() => setSelectedLeft(null)}>
						<Close />
					</IconButton>
				</Fragment>
			)}
		</NPaper>
	</Fragment>
);
// eslint-disable-next-line react/prop-types
const EntryPanel = ({ css, isShown }) => (
	<NPaper comp="div" css={css} className={css(['welcomer', isShown && 'shown'])}>
		<Typography color="secondary" variant={isMobile ? 'display3' : 'display4'}>
			WebBoard
		</Typography>
		<Typography variant="subheading">
			Welcome, please select your book from the list.
		</Typography>
	</NPaper>
);
// eslint-disable-next-line react/prop-types
const BookList = ({ css, selectedBook, books, selectBook, isShrinked, deleteBook }) => (
	<SpecialPaper className={css(['books', selectedBook && 'selected', isShrinked && 'shrinked'])}>
		<List
			className={css('list')}
			subheader={<ListSubheader className={css('subh')} component="div">Books</ListSubheader>}
		>
			{books.map(book => (
				<ListItem
					key={book.info.name}
					button
					dense={isMobile}
					component="li"
					onClick={() => selectBook(book.info.name)}
				>
					<ListItemText
						className={css('list-text')}
						primary={book.info.name}
					/>
					<ListItemSecondaryAction>
						<IconButton color="inherit" aria-label="delete" onClick={() => deleteBook(book.info.name)}>
							<Delete />
						</IconButton>
					</ListItemSecondaryAction>
				</ListItem>
			))}
		</List>
	</SpecialPaper>
);
// eslint-disable-next-line react/prop-types
const BookDetail = ({ css, selectedBook, editDiaOpen, deleteBook, selectBook }) => (
	<NPaper comp="div" css={css} className={css(['detail', selectedBook && 'selected'])}>
		<Async
			promise={Promise.resolve(selectedBook && selectedBook.preview)}
			then={img => (
				<ImageFrame img={img} title={selectedBook ? selectedBook.info.name : undefined} />
			)}
		/>
		<IconButton aria-label="close" className={css('close')} onClick={() => selectBook(null)}>
			<Close />
		</IconButton>
		<div className={css('abs-bottom-det')}>
			{selectedBook && (
				<CardContent>
					<Typography gutterBottom variant="headline" component="h2">
						{selectedBook.info.name}
					</Typography>
					<Typography variant="caption" component="p">
						{bookInfoTexter(selectedBook)}
					</Typography>
				</CardContent>
			)}
			<CardActions>
				<Button variant="flat" color="secondary" onClick={() => editDiaOpen()}>
					Edit
					<Edit className={css('text-button-icon-right')} />
				</Button>
				<Button variant="flat" color="secondary" onClick={() => deleteBook(selectedBook.info.name)}>
					Delete
					<Delete className={css('text-button-icon-right')} />
				</Button>
			</CardActions>
		</div>
	</NPaper>
);
// eslint-disable-next-line react/prop-types
const FABs = ({ css, selectedBook, enterBook }) => (
	<Fragment>
		<Zoom in={!!selectedBook}>
			<Button
				aria-label="enter book"
				variant="fab"
				color="primary"
				className={css('floating-action-button')}
				onClick={() => enterBook(selectedBook.info.name)}
			>
				<PlayArrow />
			</Button>
		</Zoom>
	</Fragment>
);

const BooksView = (props) => {
	const {
		css,

		books,
		selectedBook,
		selectedLeft,
		mainFeatures,

		selectBook,
		enterBook,
		deleteBook,

		editDiaOpen,
		setSelectedLeft,
	} = props;
	return (
		<div
			className={css('container')}
		>
			<ButtonsPanel {...{
				props,
				css,
				isShown: !selectedBook,
				selectedLeft,
				setSelectedLeft,
				mainFeatures
			}}
			/>
			<EntryPanel {...{ css, isShown: (!selectedBook) && (!selectedLeft) }} />
			<BookList {...{
				css,
				selectedBook,
				books,
				selectBook: (...x) => {
					setSelectedLeft(null);
					selectBook(...x);
				},
				isShrinked: selectedLeft,
				deleteBook,
			}}
			/>
			<BookDetail {...{ css, selectedBook, deleteBook, editDiaOpen, selectBook }} />
			<FABs {...{ css, enterBook, selectedBook }} />
		</div>
	);
};

BooksView.defaultProps = {
	selectedBook: null,
	selectedLeft: null,
};

BooksView.propTypes = {
	books: PropTypes.array.isRequired,
	selectedBook: PropTypes.object,

	selectBook: PropTypes.func.isRequired,
	enterBook: PropTypes.func.isRequired,
	deleteBook: PropTypes.func.isRequired,

	// HOCs
	css: PropTypes.func.isRequired,
	selectedLeft: PropTypes.string,
	setSelectedLeft: PropTypes.func.isRequired,
	mainFeatures: PropTypes.array.isRequired,
	editDiaOpen: PropTypes.func.isRequired,
};

export default _flow([
	withDialog(EditDialog, 'aminakoyimbensenin', {
		propName: 'editDia',
		dialogProps: {
			fullWidth: true
		}
	}),
	withState('selectedLeft', 'setSelectedLeft', null),
	withFromApp('mainFeatures'),
	withTheme({ ...style, ...commonStyles }),
])(BooksView);
