import React from 'react';
import PropTypes from 'prop-types';
import Slide from '@material-ui/core/Slide';
import CssBaseline from '@material-ui/core/CssBaseline';
import JssProvider from 'react-jss/lib/JssProvider';
import memoize from 'lodash/memoize';
import { create } from 'jss';
import { MuiThemeProvider, createMuiTheme, createGenerateClassName, jssPreset } from '@material-ui/core/styles';

import Devices from '@material-ui/icons/Devices';

import withFromApp from 'app/utils/HOCs/withFromAppHOC';
import { isMobile, stylist } from 'app/utils';
import variables, { viewTransitionEnter, viewTransitionExit } from 'app/styles/variables';
import tFile from 'app/styles/themes';

import style from './style.scss';
import './only-this-app.scss';

const css = stylist(style);

const { themes } = tFile;

if (__ELECTRON__) {
	import(/* webpackChunkName: typeface-raleway */'typeface-raleway');
	import(/* webpackChunkName: typeface-roboto */'typeface-roboto');
}

if (!isMobile) {
	import(/* webpackChunkName: scrollbar.scss */ './scrollbar.scss');
}

// const css = stylist({ ...specStyle, ...style });

const generateClassName = createGenerateClassName();
const jss = create(jssPreset());
jss.options.insertionPoint = 'jss-insertion-point'; // comment var htmlde

const defs = createMuiTheme();
const themeFn = memoize((theme) => createMuiTheme({ // BUGFIX -> memoize: fuck perf jss
	palette: {
		type: theme.type,
		contrastThreshold: 2,
		primary: {
			main: theme.primary
		},
		secondary: {
			main: theme.secondary
		},
		background: {
			default: theme.backgroundColor,
			paper: theme.backgroundColor,
		}
	},
	typography: {
		fontFamily: variables.uiFont,
		display1: {
			fontWeight: defs.typography.fontWeightMedium,
		},
		display2: {
			fontWeight: defs.typography.fontWeightMedium,
		},
		display3: {
			fontWeight: defs.typography.fontWeightMedium,
		},
		display4: {
			fontWeight: defs.typography.fontWeightMedium,
		},
	}
}));

const Connected = ({ theme }) => (
	<Devices className={css('connected', theme)} />
);

document.documentElement.setAttribute(`data-${__MEDIUM__}`, '');
document.documentElement.setAttribute(`data-${isMobile ? 'mobile' : 'desktop'}`, '');
const AppView = ({ view: View, shown, theme, isConnected }) => (
	<MuiThemeProvider theme={themeFn(themes[theme])}>
		<CssBaseline />
		<JssProvider jss={jss} generateClassName={generateClassName}>
			<Slide direction={!shown ? viewTransitionExit : viewTransitionEnter} in={shown}>
				{View}
			</Slide>
		</JssProvider>
		{isConnected && <Connected theme={theme} />}
	</MuiThemeProvider>
);

AppView.propTypes = {
	view: PropTypes.element.isRequired,
	shown: PropTypes.bool.isRequired,

	theme: PropTypes.string.isRequired,
	isConnected: PropTypes.bool.isRequired,
};

export const AppViewPure = AppView;

export default withFromApp(['theme', 'isConnected'])(AppView);
