const path = require('path');
const { BrowserWindow } = require('electron');

throw new Error('modal de istenmio');

const modal = (modalName, props, event, onClose) => {
	let child = new BrowserWindow({
		parent: global.mainWindow,
		modal: true,
		show: false
	});

	global.childWindows[modalName] = child;

	child.loadURL(`file://${path.join(global.APP_DIRNAME, 'dist/index.electron.dialog.html')}`);
	child.webContents.executeJavaScript(`global.renderModal(
		${JSON.stringify(modalName)},
		${JSON.stringify(props)}
	)`);

	child.once('closed', () => {
		child = null;
		onClose();
	});
};

module.exports = modal;
