const { BrowserWindow } = require('electron');

async function getImage(win) {
	const prom = new Promise(resolve => win.capturePage((image) => {
		const base64 = image.toDataURL();
		win.destroy();
		resolve(base64);
	}));
	return prom;
}

async function getPage(url, width, height) {
	const win = new BrowserWindow({
		show: false,
		width,
		height,
	});
	const prom = new Promise((resolve) => win.once('ready-to-show', () => {
		win.webContents.executeJavaScript('document.documentElement.scrollHeight', docHeight => {
			win.once('resize', () => {
				win.webContents.once('did-finish-load', () => {
					setTimeout(() => resolve(win), 500);
				});
				win.webContents.reload();
			});
			win.setContentSize(width, docHeight + 100); // 100 de pay olsun amq
		});
	}));
	win.loadURL(url);
	return prom;
}

// async function setDimensions(page, innerWidth, innerHeight) {
// 	return page;
// }

module.exports = {
	getImage,
	getPage,
	// setDimensions,
};
