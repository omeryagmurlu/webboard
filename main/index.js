const electron = require('electron');
const windowStateKeeper = require('electron-window-state');
const path = require('path');
const isDev = require('electron-is-dev');
const { default: installExtension, REACT_DEVELOPER_TOOLS } = require('electron-devtools-installer');

global.APP_DIRNAME = path.resolve(__dirname, '..');
global.childWindows = {};

const { app, BrowserWindow } = electron;

const createMain = () => {
	if (isDev) {
		[REACT_DEVELOPER_TOOLS].forEach(extension => {
			installExtension(extension)
				.then((name) => console.log(`Added Extension: ${name}`))
				.catch((err) => console.log('An error occurred: ', err));
		});
	}

	const mainWindowState = windowStateKeeper({
		defaultWidth: 1000,
		defaultHeight: 800
	});

	global.mainWindow = new BrowserWindow({
		x: mainWindowState.x,
		y: mainWindowState.y,
		width: mainWindowState.width,
		height: mainWindowState.height,
		minWidth: 600,
		minHeight: 300,
		toolbar: false,
		// transparent: true, // suanda calismiyor olabilirsin, ama ben seni yenerim
		show: false, // FOUC FIX
	});

	global.mainWindow.webContents.on('will-navigate', (e, url) => {
		if (url !== global.mainWindow.webContents.getURL()) {
			e.preventDefault();
			electron.shell.openExternal(url);
		}
	});

	global.mainWindow.loadURL(`file://${path.join(global.APP_DIRNAME, 'dist/index.electron.html')}`);

	// mainWindow.webContents.openDevTools();

	global.mainWindow.on('closed', () => {
		// On OS X it is common for applications and their menu bar
		// to stay active until the user quits explicitly with Cmd + Q
		if (process.platform !== 'darwin') {
			app.quit();
		}
	});

	global.mainWindow.webContents.on('did-finish-load', () => {
		mainWindowState.manage(global.mainWindow);
		global.mainWindow.show(); // FOUC FIX
		global.mainWindow.focus();
	});
};

app.on('ready', createMain);

app.on('browser-window-created', () => {
	// window.setMenu(null); // get rid of menu
});

app.on('activate', () => {
	// On OS X it's common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if (global.mainWindow === null) {
		createMain();
	}
});

require('./ipc.js');
