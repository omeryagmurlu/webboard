const { ipcMain: ipc, BrowserWindow } = require('electron');
const path = require('path');

const webToImg = require('./workerWindows/webpageToImage');
// const modal = require('./modal');

const makeZombie = (childName, cb = () => {}) => {
	const zombie = new BrowserWindow({
		show: false
	});
	zombie.on('ready-to-show', () => {
		// zombie.show();
		cb(zombie);
	});
	zombie.loadURL(`file://${path.join(global.APP_DIRNAME, `dist/index.electron.${childName}.html`)}`);

	return zombie;
};

ipc.on('webpageToImage|getImage', async (event, url, width, height) => {
	const page = await webToImg.getPage(url, width, height);
	// await webToImg.setDimensions(page, width, height);
	const img = await webToImg.getImage(page);

	event.sender.send(`Re:webAction|getImage:arg(${url})`, img);
});

let serverRenderer;
ipc.on('connection|init', async (event, port) => {
	if (serverRenderer) {
		serverRenderer.destroy();
	}

	serverRenderer = makeZombie('server-renderer', zombie => {
		zombie.webContents.send('listen-server', port);
		event.sender.send(`Re:connection|init:port(${port})`);
	});
});

ipc.on('connection|kill', async (event) => {
	if (serverRenderer) {
		serverRenderer.close();
	}

	event.sender.send('Re:connection|kill');
});

ipc.on('connection|send', async (event, ...x) => {
	if (serverRenderer) {
		serverRenderer.webContents.send('sendMessage', ...x);
	}
});

// const modals = {};
// ipc.on('modal:open', async (event, id, modalName, props) => {
// 	if (modals[id]) {
// 		throw new Error('Modal exist madafaka');
// 	}
//
// 	modals[id] = modal(modalName, props, event, (data) => {
// 		event.sender.send(`Re:modal:arg(${id}):close`, true, data);
// 	});
// });
//
// ipc.on('modal:close', async (event, id) => {
// 	modals[id]();
// 	modals[id] = undefined;
// });
