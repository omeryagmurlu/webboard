import React, { Component } from 'react';
import io from 'socket.io-client';
import platform from 'platform';

import Typography from '@material-ui/core/Typography';

import { AppViewPure } from 'app/views/AppView';
import { stylist } from 'app/utils';
import 'app/views/AppView/style.scss';

import Board from 'appremote/Board';
import Books from 'appremote/Books';
import style from './style.scss';

const css = stylist(style);

class App extends Component {
	static processGenericPayload = pay => ({
		theme: pay
	})

	constructor(props) {
		super(props);

		this.state = {
			view: undefined,
			wirings: [],
			remoteState: undefined,

			message: 'While board is awaiting a remote, refresh this page to connect.',
			theme: 'giant-goldfish'
		};

		this.socket = io();
		this.socket.on('connect', () => { // bu server-renderer bagi
			this.socket.emit('!#!#special!#!# - info', platform);
		});
	}

	componentDidMount() {
		this.socket.on('device-accepted', payload => {
			this.setState({
				message: 'Connected! Remote will activate when suitable.',
				...App.processGenericPayload(payload),
			});
		});

		this.socket.on('common-payload', payload => {
			this.setState({
				...App.processGenericPayload(payload)
			});
		});

		this.socket.on('device-dropped', () => {
			this.setState({
				message: 'Device disconnected! Please reconnect!'
			});
		});

		this.socket.on('updateBoard', this._switcher(Board));
		this.socket.on('updateBooks', this._switcher(null));
		this.socket.on('exitView', () => this.setState({
			view: undefined,
			wirings: [],
			remoteState: undefined,
			message: 'Remote will activate when suitable.',
		}));

		this.socket.on('wiring', wirings => this.setState({ wirings: wirings || [] }));
	}

	_switcher = (vv) => state => this.setState(prev => {
		const obj = ({
			// partial update
			remoteState: state ? prev.view !== vv ? state : { ...prev.remoteState, ...state } : undefined,
		});

		if (prev.view !== vv) obj.view = vv;
		return obj;
	});

	call = (fName, ...arg) => {
		if (this.state.wirings.indexOf(fName) !== -1) {
			this.socket.emit(fName, ...arg);
		}
	}

	render() {
		const View = this.state.view;
		return (
			<AppViewPure
				shown
				view={View ? (
					<View
						remote={this.state.remoteState}
						call={this.call}
						theme={this.state.theme}
					/>
				) : (
					<div className={css('yuzvh')}>
						<div className={css('midcenter')}>
							<Typography variant="display3">Webboard Remote</Typography>
							<Typography variant="headline">{this.state.message}</Typography>
						</div>
					</div>
				)}
				theme={this.state.theme}
			/>
		);
	}
}

export default App;
