import React from 'react';
import ReactDOM from 'react-dom';
import 'typeface-roboto/index.css';
import App from 'appremote/App';

const render = (Component) => {
	ReactDOM.render(
		<Component />,
		document.getElementById('react-binding')
	);
};

render(App);
// if (module.hot) {
// 	module.hot.accept('appremote/App', () => {
// 		render(App);
// 	});
// }
