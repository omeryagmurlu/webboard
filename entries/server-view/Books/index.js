import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import { withState, withHandlers, compose } from 'recompose';

import ImageFrame from 'app/components/ImageFrame';

import Expansion from 'appremote/components/Expansion';

import { stylist } from 'app/utils';

import style from './style.scss';

let css;
const rawCss = stylist(style);
let theme;

const Books = ({
	remote: {
		books = []
	},
	call,
	theme: raTh,
	expanded,
	changePanel
}) =>
	(css = (x) => rawCss(x, theme)) &&
	(theme = raTh) &&
(
	<div className={css('container')}>
		<Typography className={css('heading')} variant="display3">Books</Typography>
		<Typography className={css('heading')} variant="caption">{books.length} book{books.length > 1 ? 's' : ''}</Typography>
		{books.map(book => (
			<Expansion
				expanded={expanded}
				title={book.name}
				changePanel={changePanel}
			>
				<ExpansionPanelDetails className={css('panel')}>
					<ImageFrame
						img={book.pages[book.currPageNum].pageImage}
						title={book.name}
					/>
					<pre>{JSON.stringify(book, undefined, '  ')}</pre>
				</ExpansionPanelDetails>
			</Expansion>
		))}
	</div>
);

Books.propTypes = {
	remote: PropTypes.object.isRequired,
	call: PropTypes.func.isRequired,
	theme: PropTypes.string.isRequired,
	// recompose
	expanded: PropTypes.string.isRequired,
	changePanel: PropTypes.func.isRequired,
};

export default compose(
	withState('expanded', 'setExpanded'),
	withHandlers({
		changePanel: ({ setExpanded }) => panel => (e, isExpanded) =>
			setExpanded(isExpanded ? panel : null),
	}),
)(Books);
