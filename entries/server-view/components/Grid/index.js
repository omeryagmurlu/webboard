import React from 'react';
import PropTypes from 'prop-types';
import GridList from '@material-ui/core/GridList';
import ButtonBase from '@material-ui/core/ButtonBase';
import Paper from '@material-ui/core/Paper';

import Check from '@material-ui/icons/Check';

import { stylist, noop } from 'app/utils';

import style from './style.scss';

let css;
const raCss = stylist(style);

const Grid = ({ elems, acPred, buttonProps, checkColor, children, theme }) =>
	(css = (...x) => raCss(...x, theme)) &&
(
	<GridList cellHeight="auto" className={css('grid-list')}>
		{elems.map(elem => {
			const { className = '', ...bProps } = buttonProps(elem);
			return (
				<div className={css('grid-item-cont')}>
					<ButtonBase
						component={Paper}
						className={`${css([
							'grid-item',
							acPred(elem) && 'active'
						])} ${className}`}
						key={elem}
						{...bProps}
					>
						<div className={css('inneropa')} />
						{children(elem)}
						{acPred(elem) && <Check
							className={css('col-check-icon')}
							style={{
								color: checkColor(elem)
							}}
						/>}
					</ButtonBase>
				</div>
			);
		})}
	</GridList>
);

Grid.defaultProps = {
	checkColor: noop,
	children: noop,
	acPred: () => false,
	buttonProps: () => ({}),
};

Grid.propTypes = {
	checkColor: PropTypes.func,
	children: PropTypes.func,
	acPred: PropTypes.func,
	elems: PropTypes.array.isRequired,
	buttonProps: PropTypes.func,
};

export default Grid;
