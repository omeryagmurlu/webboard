import Grid from './index';

export default {
	component: Grid,
	props: JSON.parse('{"theme":"sugar","elems":["#000000","#FFFFFF","#FF0000","#004080","#008000","#FFFF00","#C87400","#800040","#008080","#5F2D0A"]}')
};
