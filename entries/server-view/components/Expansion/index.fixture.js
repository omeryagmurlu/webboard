import Expansion from './index';

export default {
	component: Expansion,
	props: {
		expanded: 'Yolo',
		changePanel: () => {},
		title: 'Yolo',
		caption: 'I do always',
		children: 'May I be your content',
		theme: 'giant-goldfish'
	}
};
