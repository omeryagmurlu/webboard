import React from 'react';
import PropTypes from 'prop-types';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';

import ExpandMore from '@material-ui/icons/ExpandMore';

import { stylist } from 'app/utils';

import style from './style.scss';

let css;
const raCss = stylist(style);

const Expansion = ({ expanded, changePanel, children, title, caption, theme }) =>
	(css = (x) => raCss(x, theme)) &&
(
	<ExpansionPanel className={css('expan')} expanded={expanded === title} onChange={changePanel(title)}>
		<ExpansionPanelSummary className={css('expan-sum')} expandIcon={<ExpandMore className={css('col-backcol')} />}>
			<Typography className={css('exp-heading')}>{title}</Typography>
			<Typography className={css('col-backcol')} variant="caption">{caption}</Typography>
		</ExpansionPanelSummary>
		{children}
	</ExpansionPanel>
);

Expansion.defaultProps = {
	expanded: undefined,
	children: null,
	title: '',
	caption: '',
};

Expansion.propTypes = {
	expanded: PropTypes.string,
	changePanel: PropTypes.func.isRequired,
	title: PropTypes.string,
	caption: PropTypes.string,
	children: PropTypes.any,
};

export default Expansion;
