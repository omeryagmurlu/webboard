import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { compose, withHandlers, onlyUpdateForKeys } from 'recompose';
import chroma from 'chroma-js';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';

import StrokeMeter from 'app/components/StrokeMeter';
import BottomPaginator from 'app/components/BottomPaginator';
import ImageFrame from 'app/components/ImageFrame';
import { stylist, toolIconProcess, bookInfoTexter } from 'app/utils';
import colors from 'app/data/colors';
import { toolsObject } from 'app/data/tools';
import { stateHOC } from 'app/utils/HOCs';

import Grid from 'appremote/components/Grid';
import Expansion from 'appremote/components/Expansion';

import style from './style.scss';

let css;
const rawCss = stylist(style);
let theme;

const ColorPalette = onlyUpdateForKeys(
	['curCol']
)(({ onClick, curCol }) => (
	<Grid
		theme={theme}
		elems={colors}
		acPred={color => chroma(color).hex() === chroma(curCol).hex()}
		buttonProps={color => ({
			style: {
				backgroundColor: color
			},
			onClick: () => onClick(color),
		})}
		checkColor={color => chroma.mix(color, chroma(color).luminance() > 0.5 ? 'black' : 'white')}
	/>
));

const Tool = onlyUpdateForKeys(
	['toolName']
)(({ remote, call }) => (
	<Grid
		theme={theme}
		elems={remote.enabledTools.map(name => toolsObject[name])}
		acPred={tool => tool.name === remote.tool}
		buttonProps={tool => ({
			onClick: () => call('changeTool', tool.name),
		})}
	>
		{tool => {
			const { fills, svgProps, Icon, iconStyle } = toolIconProcess(tool, remote.color);
			return (
				<Fragment>
					<Icon
						{...fills}
						className={css(['icon-svg', 'tool-icon'])}
						style={{
							filter: `saturate(1.50) sepia(0.3) brightness(0.9) drop-shadow(0 0 15px ${fills.fill1})`,
							...iconStyle,
						}}
						{...svgProps}
					/>
					<Typography variant="caption" className={css('tool-name')}>{tool.name}</Typography>
				</Fragment>
			);
		}}
	</Grid>
));
// "changeTool", "changeStroke", "changePage", "exitBoard", "insertPage"
const Board = ({ remote, call, theme: raTh, state, changePanel, }) =>
	(css = (x) => rawCss(x, theme)) &&
	(theme = raTh) &&
(
	<div className={css('container')}>
		<Typography className={css('heading')} variant="display3">Board</Typography>
		<Card className={css('status')}>
			<CardHeader
				classes={{
					subheader: css('header-sub'),
					title: css('header-tit')
				}}
				title={remote.book.name}
				subheader={bookInfoTexter(remote.book)}
			/>
			<CardContent>
				<ImageFrame
					img={remote.book.pages[remote.book.currPageNum].pageImage}
					title={remote.book.name}
					className={css('frame')}
				/>
			</CardContent>
			<CardActions className={css('actions')}>
				<Button color="secondary" variant="raised" onClick={() => call('exitBoard')}>Exit</Button>
			</CardActions>
		</Card>
		<Expansion
			title="Tools"
			caption="Change the current tool"
			{...{ expanded: state.expanded, theme, changePanel }}
		>
			<ExpansionPanelDetails className={css('grid-cont')}>
				<Tool {...{ remote, call, toolName: remote.tool }} />
				<Typography variant="caption" className={css('cap')}>Tool</Typography>
			</ExpansionPanelDetails>
		</Expansion>
		<Expansion
			title="Colors"
			caption="Change the current primary and secondary colors"
			{...{ expanded: state.expanded, theme, changePanel }}
		>
			<ExpansionPanelDetails className={css(['grid-cont', 'color-cont'])}>
				<div className={css('colors-primary')}>
					<ColorPalette
						onClick={col => call('changeColor', col, 'primary')}
						curCol={remote.color.primary}
					/>
					<Typography variant="caption" className={css('cap')}>Primary Color</Typography>
				</div>
				<div className={css('colors-secondary')}>
					<ColorPalette
						onClick={col => call('changeColor', col, 'secondary')}
						curCol={remote.color.secondary}
					/>
					<Typography variant="caption" className={css('cap')}>Secondary Color</Typography>
				</div>
			</ExpansionPanelDetails>
		</Expansion>
		<Expansion
			title="Stroke"
			caption="Set the stroke width"
			{...{ expanded: state.expanded, theme, changePanel }}
		>
			<ExpansionPanelDetails classes={{ root: css('stroke-cont-out') }}>
				<StrokeMeter
					className={css('stroke')}
					onClick={(val) => call('changeStroke', val || 1)}
					currStroke={remote.tools[remote.tool].stroke}
				/>
				<Typography variant="caption" className={css('cap')}>Stroke Width</Typography>
			</ExpansionPanelDetails>
		</Expansion>
		<Expansion
			title="Pagination"
			{...{ expanded: state.expanded, theme, changePanel }}
		>
			<ExpansionPanelDetails classes={{ root: css('paginator') }}>
				<BottomPaginator {...{
					currPage: remote.book.currPageNum,
					totalPage: remote.book.info.totalPage,
					changePage: (...x) => call('changePage', ...x),
					addNewPage: remote.book.isDynamic && ((...x) => call('insertPage', ...x))
				}}
				/>
			</ExpansionPanelDetails>
		</Expansion>
	</div>
);

// <Expansion
// 	title="Preview"
// 	{...{ expanded: state.expanded, theme, changePanel }}
// >
// 	<ImageFrame img={remote.book.pages[remote.book.currPageNum].pageImage} />
// </Expansion>

Board.propTypes = {
	remote: PropTypes.object.isRequired,
	call: PropTypes.func.isRequired,
	theme: PropTypes.string.isRequired,
	// recompose
	state: PropTypes.object.isRequired,
	changePanel: PropTypes.func.isRequired,
};

export default compose(
	stateHOC({
		expanded: null
	}, 'state', 'setState'),
	withHandlers({
		changePanel: ({ setState }) => panel => (e, isExpanded) => setState({
			expanded: isExpanded ? panel : null
		}),
	}),
)(Board);
