import { ipcRenderer as ipc, remote } from 'electron';
import express from 'express';
import http from 'http';
import socketio from 'socket.io';
import path from 'path';
import isDev from 'electron-is-dev';
import isAsar from 'electron-is-running-in-asar';

const app = express();
const server = http.Server(app);
const io = socketio(server);
const mainWindow = remote.getGlobal('mainWindow');

const actPath = 'dist/server-public';
const rootPath = !isDev
	? path.resolve(process.resourcesPath, isAsar() ? 'app.asar.unpacked' : 'app', actPath)
	: actPath;

app.get('/', (req, res) => {
	res.sendFile('index.electron.server-view.html', { root: rootPath });
});
app.use(express.static(rootPath));

ipc.on('listen-server', (event, port) => {
	server.listen(port, () => {
		console.log(`listening on *:${port}`);
	});
});

io.on('connection', (socket) => {
	socket.on('!#!#special!#!# - info', info => { // infoyu aldiktan sonra basliyoruz
		// burada bir check olmali ama siktir et
		mainWindow.webContents.send('connection|socketConnected', socket.id, info);
	});
	socket.use((packet, next) => {
		console.log(packet);
		mainWindow.webContents.send(`connection|packet:socket(${socket.id})`, packet);
		next();
	});
	socket.on('disconnect', (reason) => {
		mainWindow.webContents.send(`connection|disconnect:socket(${socket.id})`, reason);
	});
});

ipc.on('sendMessage', (event, sockId, message, ...arg) => {
	io.to(sockId).emit(message, ...arg);
});
